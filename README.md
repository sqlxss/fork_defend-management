# defend-management

#### 介绍
当前仓库为 defend 组的信息记录、发布仓库。

具体详细内容，可移步 WiKi ，后续相关内容将在 WiKi 更新。

WiKi 地址： [WiKi](https://gitee.com/openkylin/defend-management/wikis/Home)

目前代码仓库还没有一个比较好的位置，所以代码先放到当前这个仓库下了