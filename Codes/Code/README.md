# 这里放的是项目代码相关的部分

目录及内容如下

|目录名|项目说明|项目用途||
|:---:|:---:|:---:|:---:|
|Common|公库部分代码||||
|CoreModule|项目核心模块代码|存放核心模块的代码，核心模块作为主支持模块存在||
|Agent|存放Defender Agent端加载器源码|内部集成了若干Loader相关的功能，作为Agent端入口||
|Tray|存放Defender Tray端加载器源码|内部集成了若干Loader相关的功能，作为Service统一入口||
|Driver|驱动代码目录||||
|Cloud|云端代码目录||||



# 四端说明

原计划代码分四端，Tray、Agent、Driver、Cloud，原计划除了云端之外，一份代码三端通用，目前由于Driver 端开发成本较高，所以目前框架部分先由上层双端来处理解决，即目前四端中，当前代码只为 Tray 端和 Agent 端开发，为双端通用。目前的开发状态，以Agent 端为主，后续视情况来看Tray端如何实现和提供。


# 项目编译方法

位于当前目录下，首先编译 common 公库，命令

`make common_build`

之后，公库会被放到指定位置


然后根据需求，编译不同的项目，如

如果需要编译 Agent ，则可输入命令

`make agent_build`

之后会在 Agent/Outs 目录下生成一个 elf 文件，这个文件就是我们最终的结果文件


如果需要清理项目，则可以输入

`make clear`

即可，无用的内容就全部被删除了


不同模块可以单独clear 具体细节可以看 makefile


# 完整测试方法

1：编译 common

`make common_build`

2：编译 agent

`make agent_build`

3：编译所有模块

`make module_build`

4：打包软件包

`make pack`

5：测试执行 agent

`make agent_run`

6：删除编译中间文件

`make clear`

7：删除打包后的目录

`make pack_clear`

8：如果需要编译调试模式，那么需要在所有编译命令，就是1 2 3 三步后追加一条参数，添加调试参数
`DBG=-g`

# 开发过程中遇到的问题

1：开发过程中 Common 头文件的引用

Common 头文件的引用，尽量引用 经过 `make common_build` 之后的 include 目录中的头文件，

目前项目已经配置好了位置，所以只需要引一下目录即可，

所以在开发 CoreModule 和 其他模块的时候，可以先把 common 编译出来，makefile 会把头文件放到特定的位置，


2：驱动的编译命令

由于驱动文件所在目录层次有点深，所以owner 写了一个 active_build 命令来动态寻找驱动代码目录下的文件，

然后动态生成要build 的obj 文件，最后打包成一个 ko,

所以这里驱动部分编译的话，建议使用 `make active_build` 或者直接用 `make build`



