#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include "com_memory.h"
#include "com_device.h"
#include "com_log.h"
#include "com_module.h"
#include "com_path.h"

int Start(void* pEnv);

int main()
{
	//	程序启动
	std::string strDir = "";

	uint32_t nRet = zoo::common::path::GetProcessFileDir(strDir);

	if (nRet != 0)
	{
		return nRet;
	}

	std::string strPath = strDir.append("defender_core.edl");

	zoo::common::module::CLibrary cl;

	nRet = cl.Load(strPath.c_str());

	if (nRet != 0)
	{
		return nRet;
	}

	decltype(Start)* fp = NULL;

	nRet = cl.GetProc("_", (void**)&fp);

	if (nRet != 0)
	{
		return nRet;
	}

	if (fp == NULL)
	{
		return -1;
	}

	return fp(NULL);
}
