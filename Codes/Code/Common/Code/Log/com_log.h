/**
 * @file com_log.h
 * @author sqlxss (276793422@qq.com)
 * @brief
 * @version 0.1
 * @date 2023-04-23
 *
 * Copyright (c) 2023 sqlxss(zhaojianzu)
 *
 */

#include "com_base.h"
#pragma once

namespace zoo
{
    namespace common
    {
        namespace log
        {
            /**
             * @brief 这个枚举标记Log级别，后续会用到
             *
             */
            enum ENUM_LOG_LEVEL
            {
                ENUM_LOG_LEVEL_NONE = 0,
                ENUM_LOG_LEVEL_ERROR,
                ENUM_LOG_LEVEL_TRACE,
                ENUM_LOG_LEVEL_DEBUG,
                ENUM_LOG_LEVEL_INFO,
                ENUM_LOG_LEVEL_LOG,
                ENUM_LOG_LEVEL_MAX,
            };

            //  ZooTODO:
            //      这个应该是个单例，后续再修改
            //      这里目前还不完善，后续补全
            class CLog
            {
            public:
                CLog(ENUM_LOG_LEVEL eLevelMax, const char *szLogFile);

                ~CLog();

                CLog();

                void Init(ENUM_LOG_LEVEL eLevelMax, const char *szLogFile);

                void Write(ENUM_LOG_LEVEL eLevel, const char *szMessage);

                void WriteLog(const char *szMessage);

            private:
                ENUM_LOG_LEVEL _eLevelMax;
                std::string _strLogFile;
            };

        }
    }
}


#ifdef _MAIN_TEST_
namespace zoo
{
    namespace common
    {
        namespace log
        {
            namespace test
            {
                void MainTest();
            }
        }
    }
}
#endif  //_MAIN_TEST_



