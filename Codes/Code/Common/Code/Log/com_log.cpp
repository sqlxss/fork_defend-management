#include <stdio.h>
#include "com_log.h"

namespace zoo
{
    namespace common
    {
        namespace log
        {
            CLog::CLog(ENUM_LOG_LEVEL eLevelMax, const char *szLogFile)
            {
                Init(eLevelMax, szLogFile);
            }

            CLog::~CLog()
            {

            }

            CLog::CLog()
            {

            }

            void CLog::Init(ENUM_LOG_LEVEL eLevelMax, const char *szLogFile)
            {
                _eLevelMax = eLevelMax;
                _strLogFile = szLogFile;
            }

            void CLog::Write(ENUM_LOG_LEVEL eLevel, const char *szMessage)
            {
                printf("[ TIME ][ PID ][ TID ][%2d][ FLAG ] %s \n", eLevel, szMessage);
            }

            void CLog::WriteLog(const char *szMessage)
            {
                Write(ENUM_LOG_LEVEL_LOG, szMessage);
            }
        }
    }
}


#ifdef _MAIN_TEST_
namespace zoo
{
    namespace common
    {
        namespace log
        {
            namespace test
            {
                void MainTest()
                {
                    zoo::common::log::CLog _log;
                   	_log.WriteLog(std::string("open device [/dev/defender_sqlxss] dev.IsReady() : ").append(std::to_string(dev.IsReady())).c_str());
                }
            }
        }
    }
}
#endif  //_MAIN_TEST_



