/**
 * @file com_pool.h
 * @author sqlxss (276793422@qq.com)
 * @brief
 * @version 0.1
 * @date 2023-07-22
 *
 * Copyright (c) 2023 sqlxss (zhaojianzu)
 *
 */

#include "com_base.h"
#include "com_structure.h"
#include "com_pool.interface.h"
#pragma once

namespace zoo
{
	namespace common
	{
		namespace pool
		{
			/**
			 * @brief 池对象
			 */
			struct STRUCT_POOL_OBJECT
			{
				uint16_t uMajor;					///	组ID
				uint16_t uMinor;					///	子类型 ID
				uint32_t uID;						///	plugin ID
				structure::guid::GUID sGUID;		///	独立 GUID
				void* pv;							///	成员对象
			};

			void InitObject(STRUCT_POOL_OBJECT*, uint16_t, uint16_t, uint32_t, const structure::guid::GUID*, void*);
			void InitObject(STRUCT_POOL_OBJECT*, const structure::guid::GUID*, void*);

			/**
			 * 池代码，内部功能仅为入池、查询
			 */
			class CPool
			{
			public:
				CPool();

				~CPool();

				/**
				 * @brief 通过GUID做入池操作
				 */
				lerror Insert(const structure::guid::GUID* sGUID, void* pv);

				/**
				 * @brief 通过池成员信息做入池操作
				 */
				lerror Insert(uint16_t uMajor, uint16_t uMinor, uint32_t uID, const structure::guid::GUID* sGUID, void* pv);

				/**
				 * @brief 通过GUID查询池对象，找到指定GUID对应的对象
				 */
				lerror Query(const structure::guid::GUID* sGUID, void*& pv);


				/**
				 * @brief 通过池成员组信息，查询指定组的池成员，返回池成员指针列表
				 */
				lerror Query(uint16_t uMajor, uint16_t uMinor, uint32_t uID, void** pvArray, uint32_t& nCount);

				/**
				 * @brief 获取组内全部对象
				 * @param pvArray 一个对象数组，内部包含一组对象指针
				 * @param nCount 作为传入参数时，为Array 最大数量，传出时，为找到的成员数量
				 * @return lerror
				 */
				lerror GetAllMajorObject(uint16_t uMajor, void** pvArray, uint32_t& nCount);

			private:

				/**
				 * 索引是 GUID 字符串
				 * 对象是指针
				 * 这个对象不用加锁，是因为这个对象本身不允许动态插入，
				 * 这个对象的初始化，是主线程初始化所有模块的时候，同步初始化的，不涉及到竞争的问题，
				 */
				std::map<structure::guid::GUID, const STRUCT_POOL_OBJECT*> _pool;
			};

			/**
			 * 池管理器代码，内部功能为 创建一个池、查询一个池
			 */
			class CPoolManager
			{
			public:
				CPoolManager();

				~CPoolManager();

				/**
				 * @brief 根据UUID创建一个池对象，如果存在，则失败
				 */
				lerror Create(const structure::guid::GUID* sGUID);

				/**
				 * @brief 根据UUID创建一个池对象，如果存在，则直接获取
				 */
				lerror Create(const structure::guid::GUID* sGUID, CPool*& pv);

				/**
				 * @brief 根据一个UUID查询一个池对象
				 */
				lerror Query(const structure::guid::GUID* sGUID, CPool*& pv);

			private:

				/**
				 * 这个对象不用加锁，是因为这个对象本身不允许动态插入，
				 * 这个对象的初始化，是主线程初始化所有模块的时候，同步初始化的，不涉及到竞争的问题，
				 */
				std::map<structure::guid::GUID, CPool*> _pool;
			};

			class CPoolControler : public IPoolControler
			{
			public:
				CPoolControler();

				~CPoolControler();

				lerror Init();

				void SetInited();

			public:

				/**
				 * @brief 向指定池内插入一个对象
				 */
				virtual lerror InsertObject(const structure::guid::GUID* sPool, uint16_t uMa, uint16_t uMi, uint32_t uId, const structure::guid::GUID* sObject, void* pv);

				/**
				 * @brief 从池中寻找指定对象
				 */
				virtual lerror Query(const structure::guid::GUID* sPool, const structure::guid::GUID* sUUID, void*& pv);

				/**
				 * @brief 从池中批量寻找对象，正常来说，这里是用不到的
				 */
				virtual lerror Query(const structure::guid::GUID* sPool, uint16_t uMajor, uint16_t uMinor, uint32_t uID, void** pvArray, uint32_t& nCount);

			private:

				CPoolManager _manager;		/// 池管理器

				uint32_t _initing;
			};

			/**
			 * @brief 加载一个本地模块
			 */
			lerror PoolObjectLoader(const char* szPath, const void* pParam);

		}
	}
}
