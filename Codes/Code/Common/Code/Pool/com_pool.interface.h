/**
 * @file com_pool.interface.h
 * @author sqlxss (276793422@qq.com)
 * @brief
 * @version 0.1
 * @date 2023-07-23
 *
 * Copyright (c) 2023 sqlxss (zhaojianzu)
 *
 */


#include "com_structure.h"
#pragma once


namespace zoo
{
	namespace common
	{
		namespace pool
		{
			//	第一个池，是个 Function 池，内部存放所有 Function 功能
			//	{00000001-0002-0003-0000-000000000001}
			static DEFINE_GUID(POOL_OBJECT_FUNCTION, 1, 2, 3, 0, 0, 0, 0, 0, 0, 0, 1);

			class IPoolControler
			{
			public:

				/**
				 * @brief 向指定池内插入一个对象
				 */
				virtual lerror InsertObject(const structure::guid::GUID* sPool, uint16_t uMa, uint16_t uMi, uint32_t uId, const structure::guid::GUID* sObject, void* pv) = 0;

				/**
				 * @brief 从池中寻找指定对象
				 */
				virtual lerror Query(const structure::guid::GUID* sPool, const structure::guid::GUID* sUUID, void*& pv) = 0;

				/**
				 * @brief 从池中批量寻找对象
				 */
				virtual lerror Query(const structure::guid::GUID* sPool, uint16_t uMajor, uint16_t uMinor, uint32_t uID, void** pvArray, uint32_t& nCount) = 0;

			};

		}
	}
}














