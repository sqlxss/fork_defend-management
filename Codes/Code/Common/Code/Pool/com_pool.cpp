#include "com_pool.h"
#include "com_memory.h"
#include "com_module.h"

namespace zoo
{
	namespace common
	{
		namespace pool
		{

			void InitObject(STRUCT_POOL_OBJECT* pThis, uint16_t ma, uint16_t mi, uint32_t id, const structure::guid::GUID *gid, void *p)
			{
				pThis->uMajor = ma;
				pThis->uMinor = mi;
				pThis->uID = id;
				pThis->sGUID = *gid;
				pThis->pv = p;
			}

			void InitObject(STRUCT_POOL_OBJECT* pThis, const structure::guid::GUID *gid, void *p)
			{
				InitObject(pThis, 0, 0, 0, gid, p);
			}

			////////////////////////////////////////////////////////////////////////////////////////////////////

			CPool::CPool()
			{
				_pool.clear();
			}

			CPool::~CPool()
			{
				//	正常来说，这里需要一个一个放掉池内对象
				for (auto it : _pool)
				{
					const STRUCT_POOL_OBJECT* node = it.second;
					zoo::common::memory::FreeWithTag((void *)node, 14400);
				}
			}

			/**
			 * @brief 通过GUID做入池操作
			 */
			lerror CPool::Insert(const structure::guid::GUID *sGUID, void *pv)
			{
				return Insert(0, 0, 0, sGUID, pv);
			}

			/**
			 * @brief 通过池成员信息做入池操作
			 */
			lerror CPool::Insert(uint16_t uMajor, uint16_t uMinor, uint32_t uID, const structure::guid::GUID *sGUID, void *pv)
			{
				auto it = _pool.find(*sGUID);
				if (it != _pool.end())
				{
					return 0x00001000;
				}

				auto *pObj = (STRUCT_POOL_OBJECT *)zoo::common::memory::AllocWithTag(sizeof(STRUCT_POOL_OBJECT), 14400);
				if (pObj == NULL)
				{
					return 0x00001001;
				}

				InitObject(pObj, uMajor, uMinor, uID, sGUID, pv);

				_pool.insert(std::pair<structure::guid::GUID, const STRUCT_POOL_OBJECT *>(*sGUID, pObj));

				return 0;
			}

			/**
			 * @brief 通过GUID查询池对象，找到指定GUID对应的对象
			 */
			lerror CPool::Query(const structure::guid::GUID *sGUID, void *&pv)
			{
				auto it = _pool.find(*sGUID);
				if (it == _pool.end())
				{
					return 0x00001002;
				}
				pv = (void *)(it->second->pv);
				return 0;
			}

			/**
			 * @brief 通过池成员组信息，查询指定组的池成员，返回池成员指针列表
			 */
			lerror CPool::Query(uint16_t uMajor, uint16_t uMinor, uint32_t uID, void **pvArray, uint32_t &nCount)
			{
				//	如果要查询一个 Major 里面的所有对象，这里不关心 Minor ID
				if (uMinor == 0)
				{
					return GetAllMajorObject(uMajor, pvArray, nCount);
				}
				uint32_t i = 0;
				for (auto it : _pool)
				{
					const STRUCT_POOL_OBJECT* node = it.second;
					if (node->uMajor == uMajor && node->uMinor == uMinor)
					{
						if (uID == 0 || uID == node->uID)
						{
							if (i < nCount)
							{
								pvArray[i] = node->pv;
								i++;
							}
							else
							{
								return 0;
							}
						}
					}
				}
				nCount = i;
				return 0;
			}

			/**
			 * @brief
			 */
			lerror CPool::GetAllMajorObject(uint16_t uMajor, void** pvArray, uint32_t& nCount)
			{
				uint32_t i = 0;
				for (auto it : _pool)
				{
					const STRUCT_POOL_OBJECT* node = it.second;

					if (node->uMajor == uMajor)
					{
						if (i < nCount)
						{
							pvArray[i] = node->pv;
							i++;
						}
						else
						{
							return 0;
						}
					}
				}
				nCount = i;
				return 0;
			}

			////////////////////////////////////////////////////////////////////////////////////////////////////


			CPoolManager::CPoolManager()
			{
				_pool.clear();
			}

			CPoolManager::~CPoolManager()
			{

			}

			/**
			 * @brief 根据UUID创建一个池对象，如果存在，则失败
			 */
			lerror CPoolManager::Create(const structure::guid::GUID* sGUID)
			{
				CPool* p = new CPool();
				if (p == NULL)
				{
					return 0x00001100;
				}
				_pool.insert(std::pair<structure::guid::GUID, CPool*>(*sGUID, p));
				return 0;
			}

			/**
			 * @brief 根据UUID创建一个池对象，如果存在，则直接获取
			 */
			lerror CPoolManager::Create(const structure::guid::GUID* sGUID, CPool*& pv)
			{
				auto p = _pool.find(*sGUID);
				if (p == _pool.end())
				{
					//	没找到，创建一个
					lerror e = Create(sGUID);
					if (e != 0)
					{
						return e;
					}
					//	再找一下
					p = _pool.find(*sGUID);
				}
				if (p == _pool.end())
				{
					//	还没找到
					return 0x00001100;
				}
				pv = p->second;
				return 0;
			}

			/**
			 * @brief 根据一个UUID查询一个池对象
			 */
			lerror CPoolManager::Query(const structure::guid::GUID* sGUID, CPool*& pv)
			{
				auto p = _pool.find(*sGUID);
				if (p == _pool.end())
				{
					return 0x00001101;
				}
				pv = p->second;
				return 0;
			}

			////////////////////////////////////////////////////////////////////////////////////////////////////

			CPoolControler::CPoolControler()
			{
				Init();
			}

			CPoolControler::~CPoolControler()
			{

			}

			lerror CPoolControler::Init()
			{
				_initing = 0;

				_manager.Create(&POOL_OBJECT_FUNCTION);
				return 0;
			}

			void CPoolControler::SetInited()
			{
				_initing = 1;
			}

			/**
			 * @brief 向指定池内插入一个对象
			 */
			lerror CPoolControler::InsertObject(const structure::guid::GUID* sPool, uint16_t uMa, uint16_t uMi, uint32_t uId, const structure::guid::GUID* sObject, void* pv)
			{
				if (_initing == 1)
				{
					return 0x00001104;
				}
				CPool* pPool = NULL;
				//	先从池管理器里面找池对象
				lerror e = _manager.Query(sPool, pPool);
				if (e == 0x00001101)
				{
					//	如果没找到，那么创建一个，然后再找
					//		给后续功能一个创建自己独立池的机会
					_manager.Create(sPool);
					//	前面创建个新池，这里获取这个新池
					//		后面如果还取不到，那就没招了
					e = _manager.Query(sPool, pPool);
				}
				if (e != 0)
				{
					return 0x00001102;
				}
				if (pPool == NULL)
				{
					return 0x00001102;
				}

				//	从池对象里面找池元素
				e = pPool->Insert(uMa, uMi, uId, sObject, pv);
				if (e != 0)
				{
					return e;
				}
				//	已经存在了,找到了
				return 0;
			}

			/**
			 * @brief 从池中寻找指定对象
			 */
			lerror CPoolControler::Query(const structure::guid::GUID* sPool, const structure::guid::GUID* sUUID, void*& pv)
			{
				CPool* pPool = NULL;
				//	先从池管理器里面找池对象
				lerror e = _manager.Query(sPool, pPool);
				if (e != 0)
				{
					return 0x00001102;
				}
				if (pPool == NULL)
				{
					return 0x00001102;
				}

				//	从池对象里面找池元素
				e = pPool->Query(sUUID, pv);
				if (e != 0)
				{
					return e;
				}
				//	已经存在了,找到了
				return 0;
			}

			/**
			 * @brief 从池中批量寻找对象
			 */
			lerror CPoolControler::Query(const structure::guid::GUID* sPool, uint16_t uMajor, uint16_t uMinor, uint32_t uID, void** pvArray, uint32_t& nCount)
			{
				CPool* pPool = NULL;
				//	先从池管理器里面找池对象
				lerror e = _manager.Query(sPool, pPool);
				if (e != 0)
				{
					return 0x00001102;
				}
				if (pPool == NULL)
				{
					return 0x00001102;
				}

				//	从池对象里面找池元素
				e = pPool->Query(uMajor, uMinor, uID, pvArray, nCount);
				if (e != 0)
				{
					return e;
				}
				//	已经存在了,找到了
				return 0;
			}

			////////////////////////////////////////////////////////////////////////////////////////////////////

			lerror ModuleInit(const void* pv);

			/**
			 * @brief 加载一个本地模块
			 */
			lerror PoolObjectLoader(const char* szPath, const void* pParam)
			{
				//	先直接加载模块，理论上可以加载成功
				void* hModule = zoo::common::module::Load(szPath);
				if (hModule == NULL)
				{
					return 0x00001200;
				}
				auto fp = (decltype(ModuleInit)*)zoo::common::module::GetProcAddress(hModule, "__");
				if (fp == NULL)
				{
					return 0x00001201;
				}

				//	执行失败了需要立刻释放，不占用本地文件，下次还能加载
				lerror uRet = 0;
				try
				{
					uRet = fp(pParam);
				}
				catch (...)
				{
					uRet = 0x00001202;
				}
				if (uRet != 0)
				{
					zoo::common::module::Free(hModule);
				}

				return uRet;
			}
		}
	}
}
