#include "com_path.h"
#include <dlfcn.h>
#include <string.h>

namespace zoo
{
	namespace common
	{
		namespace path
		{
			lerror GetModulePath(void *h, char *lpBuffer, uint32_t &nLen)
			{
				if (h == NULL)
				{
					h = (void *)GetModulePath;
				}
				Dl_info dl_info;

				//  根据一个函数取模块路径
				lerror dwRet = dladdr(h, &dl_info);
				if (dwRet != 0)
				{
					dwRet = strlen(dl_info.dli_fname) + 1;

					//  Buffer 长度不够
					if (dwRet > nLen)
					{
						nLen = dwRet;
						return dwRet;
					}

					strcpy(lpBuffer, dl_info.dli_fname);
					dwRet = 0;
				}
				else
				{
					dwRet = -1;
				}
				return dwRet;
			}

			lerror GetProcessFilePath(char *lpBuffer, uint32_t &nLen)
			{
				return GetModulePath(NULL, lpBuffer, nLen);
			}

			lerror GetProcessFileDir(char *lpBuffer, uint32_t &nLen)
			{
				uint32_t dwRet = GetProcessFilePath(lpBuffer, nLen);

				if (dwRet != 0 || dwRet == nLen)
				{
					return 1;
				}

				char *st = strrchr(lpBuffer, '\\');
				if (st == NULL)
				{
					st = strrchr(lpBuffer, '/');
				}

				if (st == NULL)
				{
					return 2;
				}

				st[1] = '\0';

				nLen = (uint32_t)strlen(lpBuffer);
				return 0;
			}

			lerror GetProcessFileDir(std::string &strDir)
			{
				uint32_t nLen = 1024;
				char szBuffer[nLen] = {0};

				lerror nRet = GetProcessFileDir(szBuffer, nLen);
				if (nRet != 0)
				{
					strDir = "";
					return nRet;
				}
				strDir = szBuffer;
				return 0;
			}

			lerror GetProcessFileDirReal(std::string &strDir)
			{
				std::string strPath;
				lerror e = zoo::common::path::GetProcessFileDir(strPath);
				if (e != 0)
				{
					return e;
				}

				char szRealPath[1024] = "";
				const char* strReal = realpath(strPath.c_str(), szRealPath);
				if (strReal == NULL)
				{
					return 1;
				}

				strDir = strReal;

				strDir += "/";

				return 0;
			}

		}
	}
}
