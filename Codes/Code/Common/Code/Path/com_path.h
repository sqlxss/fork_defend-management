/**
 * @file com_path.h
 * @author sqlxss (276793422@qq.com)
 * @brief
 * @version 0.1
 * @date 2023-07-04
 *
 * Copyright (c) 2023 sqlxss(zhaojianzu)
 *
 */
#include "com_base.h"
#pragma once

namespace zoo
{
    namespace common
    {
        namespace path
        {

            lerror GetProcessFileDir(char* lpBuffer, uint32_t &nLen);

            lerror GetProcessFileDir(std::string &);

			lerror GetProcessFileDirReal(std::string &strDir);

        }
    }
}










