/**
 * @file com_base.h
 * @author sqlxss (276793422@qq.com)
 * @brief
 * @version 0.1
 * @date 2023-07-23
 *
 * Copyright (c) 2023 sqlxss (zhaojianzu)
 *
 */


#pragma once

#include <sys/io.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include <sys/file.h>
#include <sys/fcntl.h>
#include <sys/dir.h>
#include <stdio.h>
#include <stdint.h>
#include <dlfcn.h>
#include <memory.h>
#include <cstddef>
#include <map>
#include <string>
#include <vector>

typedef int lerror;

#define 	TRUE		1
#define 	FALSE		0
