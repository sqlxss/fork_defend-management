#include "com_file.h"
#include "com_buffer.h"

#include <cstring>

namespace zoo
{
    namespace common
    {
        namespace file
        {

			//	ZooTODO
			//		文件操作部分，目前我就写了这么一块
			//		多多少少还是有问题的，问题出在哪里呢，自己去猜吧

			CFile::CFile()
			{
				_strFilePath = "";
				_hFile = NULL;
			}

			CFile::~CFile()
			{
				Close();
			}

			lerror CFile::Open(const char* lpcsPath, const char* lpcsType)
			{
				if (_hFile)
				{
					return 0x00001300;
				}
				FILE* f = fopen(lpcsPath, lpcsType);
				if (f == NULL)
				{
					return 0x00001301;
				}
				_hFile = f;
				_strFilePath = lpcsPath;
				return 0;
			}

			lerror CFile::Read(void* buf, uint32_t nCount, uint32_t& nRead)
			{
				if (_hFile == NULL)
				{
					return 0x00001302;
				}
				if (feof(_hFile))
				{
					//	文件结束了，读不到了
					return 0x00001303;
				}
				nRead = (uint32_t)fread(buf, 1, (size_t)nCount, _hFile);
				return 0;
			}

			lerror CFile::ReadLine(void* buf, uint32_t nCount)
			{
				if (_hFile == NULL)
				{
					return 0x00001302;
				}
				if (feof(_hFile))
				{
					//	文件结束了，读不到了
					return 0x00001303;
				}
				char* szRet = fgets((char*)buf, nCount, _hFile);
				if (szRet == NULL)
				{
					return 0x00001304;
				}
				return 0;
			}

			lerror CFile::Write(const void* buf, uint32_t nCount, uint32_t& nWrite)
			{
				if (_hFile == NULL)
				{
					return 0x00001302;
				}
				nWrite = (uint32_t)fwrite(buf, 1, (size_t)nCount, _hFile);
				return 0;
			}

			lerror CFile::WriteString(const char* str)
			{
				uint32_t t = 0;
				uint32_t l = (uint32_t)strlen(str);
				return Write(str, l, t);
			}

			lerror CFile::WriteLine(const char* str)
			{
				uint32_t nRet = WriteString(str);
				if (nRet != 0)
				{
					return nRet;
				}
				return WriteString("\n");
			}

			lerror CFile::Size(uint64_t* pSize)
			{
				if (_hFile == NULL)
				{
					return 0x00001302;
				}
				auto nSite = ftell(_hFile);

				fseek(_hFile, 0, SEEK_END);

				*pSize = (uint64_t)ftell(_hFile);

				fseek(_hFile, nSite, SEEK_SET);

				return 0;
			}

			lerror CFile::Seek(uint64_t pSite, int nFrom)
			{
				if (_hFile == NULL)
				{
					return 0x00001302;
				}
				fseek(_hFile, (long)pSite, nFrom);

				return 0;
			}

			lerror CFile::Flush()
			{
				if (_hFile == NULL)
				{
					return 0x00001302;
				}
				fflush(_hFile);
				return 0;
			}

			lerror CFile::Close()
			{
				if (_hFile == NULL)
				{
					return 0x00001302;
				}
				fclose(_hFile);
				_hFile = NULL;
				_strFilePath = "";
				return 0;
			}

			const char* CFile::GetFilePath()
			{
				return _strFilePath.c_str();
			}

			bool CFile::IsOpen()
			{
				return _hFile != NULL;
			}

			/**
			 * @brief 读取所有行.
			 */
			lerror CFile::ReadAllLines(std::vector<std::string>& vtLines)
			{
				uint32_t nRet = 0;

				//	一行最大长度 4096
				zoo::common::buffer::CBuffer<char> cb(4096);
				char* szBuffer = cb.Get();
				vtLines.clear();

				while (1)
				{
					//	一行一行读
					nRet = ReadLine(szBuffer, 4096);
					if (nRet != 0)
					{
						break;
					}

					vtLines.push_back(szBuffer);
				}

				return 0;
			}

			/**
			 * @brief 按照给定的路径去创建文件夹
			 */
			lerror CFile::CreateDirByPath(const char* szDir)
			{
				return 0;
			}

			/**
			 * @brief 文件或者文件路径是否存在
			 */
			lerror CFile::IsPathExist(const char* lpszPathName)
			{
				if (IsFileExist(lpszPathName))
				{
					return 0;
				}
				else
				{
					return 0x00001305;
				}
			}

			/**
			 * @brief 删除指定路径的文件.
			 */
			lerror CFile::RemoveFile(const char* lpszPathName)
			{
				if (remove(lpszPathName) < 0)
				{
					return 0x00001306;
				}
				return 0;
			}

			/**
			 * @brief 标准库方式判断文件是否存在
			 */
			bool IsFileExist(const char* lpcsPath)
			{
				return access(lpcsPath, 0) == 0;
			}
        }
    }
}


