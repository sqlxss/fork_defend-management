/**
 * @file com_file.h
 * @author sqlxss (276793422@qq.com)
 * @brief
 * @version 0.1
 * @date 2023-04-23
 *
 * Copyright (c) 2023 sqlxss(zhaojianzu)
 *
 */
#include "com_base.h"
#pragma once

namespace zoo
{
    namespace common
    {
        namespace file
        {

			//	ZooTODO
			//		这里要特殊处理，微软支持9钟不同的路径格式，但是我们这里只支持两种，即上层常用的NT路径，以及驱动常用的符号链接路径，
			class CFile
			{
			public:
				CFile();
				~CFile();
				/**
				 * @brief 打开文件
				 */
				lerror Open(const char* lpcsPath, const char* lpcsType);

				/**
				 * @brief 按照长度读取文件
				 */
				lerror Read(void* buf, uint32_t nCount, uint32_t& nRead);

				/**
				 * @brief 读一行文件
				 */
				lerror ReadLine(void* buf, uint32_t nCount);

				/**
				 * @brief 按照长度写入文件
				 */
				lerror Write(const void* buf, uint32_t nCount, uint32_t& nWrite);

				/**
				 * @brief 写一个字符串
				 */
				lerror WriteString(const char* str);

				/**
				 * @brief 写一行内容
				 */
				lerror WriteLine(const char* str);

				/**
				 * @brief 获取文件的整体长度
				 */
				lerror Size(uint64_t* pSize);

				/**
				 * @brief 移动文件指针
				 */
				lerror Seek(uint64_t pSite, int nFrom);

				/**
				 * @brief 刷新文件缓存
				 */
				lerror Flush();

				/**
				 * @brief 关闭文件
				 */
				lerror Close();

				/**
				 * @brief 获取当前文件路径
				 */
				const char* GetFilePath();

				/**
				 * @brief 判断当前是否已经打开了一个文件
				 */
				bool IsOpen();

				/**
				 * @brief 读取所有行文件
				 */
				lerror ReadAllLines(std::vector<std::string>& vtLines);

				/**
				 * @brief 根据目录循环创建文件夹保证存在
				 * @note 创建目录会失败，所以内部仅循环创建五次
				 */
				lerror CreateDirByPath(const char* szDir);

				/**
				 * @brief 判断文件是否存在
				 */
				lerror IsPathExist(const char* lpszPathName);

				/**
				 * @brief 删除一个文件
				 */
				lerror RemoveFile(const char* lpszPathName);

			private:
				std::string _strFilePath;

				FILE* _hFile;
			};

			/**
			 * @brief 判断文件是否存在
			 */
			bool IsFileExist(const char* lpcsPath);
        }
    }
}



