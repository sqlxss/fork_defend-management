#include "com_string.h"

namespace zoo
{
    namespace common
    {
        namespace string
        {
            /**
             * @brief 字符串替换
             *
             * @param origin_str
             * @param replaced_str
             * @param new_str
             * @return std::string
             */
            std::string RepalceString(const char* origin_str, const char* replaced_str, const char* new_str)
            {
                std::string result_str = origin_str;
                size_t nSrcLen = strlen(replaced_str);
                size_t nNewLen = strlen(new_str);
                for (std::string::size_type pos = 0; pos != std::string::npos; pos += 1)
                {
                    pos = result_str.find(replaced_str, pos);
                    if (pos != std::string::npos)
                    {
                        result_str.replace(pos, nSrcLen, new_str);
                    }
                    else
                    {
                        break;
                    }
                }

                return result_str;
            }

            /**
             * @brief 字符替换
             *
             * @param origin_str
             * @param replaced
             * @param new_char
             * @return std::string
             */
            std::string RepalceChar(const char* origin_str, const char replaced, const char new_char)
            {
                std::string result_str = origin_str;
                for (size_t i = 0; i < result_str.length(); i++)
                {
                    if (origin_str[i] == replaced)
                    {
                        result_str[i] = new_char;
                    }
                }

                return result_str;
            }
        }
    }
}







