/**
 * @file com_string.h
 * @author sqlxss (276793422@qq.com)
 * @brief
 * @version 0.1
 * @date 2023-04-23
 *
 * Copyright (c) 2023 sqlxss(zhaojianzu)
 *
 */

#include "com_base.h"
#pragma once

namespace zoo
{
    namespace common
    {
        namespace string
        {
            /**
             * @brief 字符串替换
             *
             * @param origin_str
             * @param replaced_str
             * @param new_str
             * @return std::string
             */
            std::string RepalceString(const char* origin_str, const char* replaced_str, const char* new_str);


            /**
             * @brief 字符替换
             *
             * @param origin_str
             * @param replaced
             * @param new_char
             * @return std::string
             */
            std::string RepalceChar(const char* origin_str, const char replaced, const char new_char);
        }
    }
}










