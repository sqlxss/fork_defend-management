/**
 * @file com_buffer.h
 * @author sqlxss (276793422@qq.com)
 * @brief
 * @version 0.1
 * @date 2023-07-24
 *
 * Copyright (c) 2023 sqlxss (zhaojianzu)
 *
 */

#include "com_base.h"
#pragma once


namespace zoo
{
	namespace common
	{
		namespace buffer
		{
			/**
			 * 当前类内部所有成员均为 malloc 申请，没有构造过程
			 * 所以当前类适用于临时申请一个缓冲区，不适用于临时申请个对象
			 *
			 * 内部函数，申请参数为成员个数，所以 cbSize 需要单独计算
			 */
			template<class T>
			class CBuffer
			{
			public:
				CBuffer()
				{
					_pv = NULL;
					_uCount = 0;
					_cbSize = 0;
					_bRelease = 0;
				}

				~CBuffer()
				{
					if (_bRelease == 0)
					{
						Free();
					}
				}

				/**
				 * @brief 直接申请
				 * @param 参数为成员个数
				 * @return
				 * @warning
				 * @note
				 * @see
				 */
				CBuffer(uint32_t n)
				{
					_pv = NULL;
					_uCount = 0;
					_cbSize = 0;
					_bRelease = 0;
					Alloc(n);
				}

				/**
				 * @brief 设置一块内存，给内部对象
				 * @param 参数为成员个数
				 * @return
				 * @warning
				 * @note 这里设置了之后，内部就不用
				 * @see
				 */
				lerror SetInit(T* p, uint32_t uCount)
				{
					if (_pv != NULL)
					{
						return 1;
					}
					if (p == NULL)
					{
						return 1;
					}
					_uCount = uCount;
					_cbSize = uCount * sizeof(T);
					_pv = p;
					return 0;
				}

				/**
				 * @brief 直接申请
				 * @param 参数为成员个数
				 * @return
				 * @warning
				 * @note
				 * @see
				 */
				lerror Alloc(uint32_t n)
				{
					if (_pv != NULL)
					{
						return 1;
					}
					_uCount = n;
					_cbSize = n * sizeof(T);
					_pv = (T*)malloc(_cbSize);
					if (_pv == NULL)
					{
						_uCount = 0;
						_cbSize = 0;
						return 1;
					}
					//	ZooTODO
					//		这里目前的处理方式是内存清空
					//		这个会导致程序执行缓慢，后续如果优化效率的话，最好别这么搞
					Zero();
					return 0;
				}

				/**
				 * @brief 直接释放内部的内存空间
				 * @param
				 * @return
				 * @warning
				 * @note
				 * @see
				 */
				lerror Free()
				{
					if (_pv == NULL)
					{
						return 1;
					}
					free(_pv);
					_pv = NULL;
					_uCount = 0;
					_cbSize = 0;
					return 0;
				}

				/**
				 * @brief 重载为成员对象指针类型
				 * @param
				 * @return
				 * @warning
				 * @note
				 * @see
				 */
				operator T* ()
				{
					return _pv;
				}

				/**
				 * @brief 重载为 uint32_t 类型
				 * @param
				 * @return 返回为字节数
				 * @warning
				 * @note
				 * @see
				 */
				operator uint32_t()
				{
					return _cbSize;
				}

				/**
				 * @brief 获取内部指针，返回对象指针类型
				 * @param
				 * @return
				 * @warning
				 * @note
				 * @see
				 */
				T* Get()
				{
					return _pv;
				}

				/**
				 * @brief 内存清空，如果存在，则直接清空
				 * @param
				 * @return
				 * @warning
				 * @note
				 * @see
				 */
				void Zero()
				{
					if (_pv)
					{
						memset((void *)_pv, 0, _cbSize);
					}
				}

				/**
				 * @brief 获取内部buffer字节数
				 * @param
				 * @return 内部buffer 字节数
				 * @warning
				 * @note
				 * @see
				 */
				uint32_t GetSize()
				{
					return _cbSize;
				}


				/**
				 * @brief 获取内部成员个数
				 * @param
				 * @return 成员数量
				 * @warning
				 * @note 这里不是字节数，因为一些时候，传入的类型可能不是对象，所以大多数情况下，字节数和成员数是相同的，
				 *       但是如果传入的类型是对象，那么就需要分开获取了
				 * @see
				 */
				uint32_t GetCount()
				{
					return _uCount;
				}

				/**
				 * @brief 判断内部指针是否为空
				 * @param
				 * @return true 是
				 * @warning
				 * @note
				 * @see
				 */
				bool IsNULL()
				{
					return _pv == NULL;
				}

				/**
				 * @brief 这个函数调用之后，析构的时候不会自动释放内存，
				 *        Free 函数支持多次调用所以释放本身没问题，
				 * @param 参数为成员个数
				 * @return
				 * @warning
				 * @note 这个函数给当前 class 一个机会，析构时不要释放内部内存，
				 *       大多数情况下用不到这个函数的功能，
				 *       但是少部分情况下，需要扒内存取出来继续使用，所以这里留了一个出口，
				 * @see
				 */
				void Release()
				{
					_bRelease = 1;
				}

			private:
				T* _pv;

				uint32_t _cbSize;		///< 字节数

				uint32_t _uCount;		///< 成员个数

				uint32_t _bRelease;		///< 外部是否已经释放
			};

			//	这里直接定义一个类型给外部用吧。
			using CBufferA = CBuffer<char>;
			using CBufferByte = CBuffer<unsigned char>;
		}
	}
}


