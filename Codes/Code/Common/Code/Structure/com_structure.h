/**
 * @file com_structure.h
 * @author sqlxss (276793422@qq.com)
 * @brief
 * @version 0.1
 * @date 2023-07-06
 *
 * Copyright (c) 2023 sqlxss(zhaojianzu)
 *
 */

#include <com_base.h>
#pragma once



#define DEFINE_GUID(name, l, w1, w2, b1, b2, b3, b4, b5, b6, b7, b8) \
        const zoo::common::structure::guid::GUID name = { l, w1, w2, { b1, b2,  b3,  b4,  b5,  b6,  b7,  b8 } }

namespace zoo
{
    namespace common
    {
        namespace structure
        {
            namespace guid
            {
                #pragma pack(1)
                //  GUID 结构，1字节对齐
                struct _GUID {
                    unsigned int   Data1;
                    unsigned short Data2;
                    unsigned short Data3;
                    unsigned char  Data4[8];

                    bool operator==(const _GUID& param) const;

                    bool operator>(const _GUID& param) const;

                    bool operator<(const _GUID& param) const;
                };
                typedef struct _GUID GUID;
                #pragma pack()

                int Cmp(const GUID* p1, const GUID* p2);

                bool IsEqual(const GUID* p1, const GUID* p2);

                std::string ToString(const GUID* g);
            }

        }
    }
}


#ifdef _MAIN_TEST_
namespace zoo
{
    namespace common
    {
        namespace structure
        {
            namespace guid
            {
                namespace test
                {
                    void MainTest();
                }
            }
        }
    }
}
#endif  //_MAIN_TEST_






