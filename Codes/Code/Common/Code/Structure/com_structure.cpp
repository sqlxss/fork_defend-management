#include "com_structure.h"
#include <string.h>
#include <string>



namespace zoo
{
    namespace common
    {
        namespace structure
        {
            namespace guid
            {

                bool _GUID::operator==(const _GUID& param) const
                {
                    return IsEqual(this, &param);
                }

                bool _GUID::operator>(const _GUID& param) const
                {
                    return Cmp(this, &param) > 0;
                }

                bool _GUID::operator<(const _GUID& param) const
                {
                    return Cmp(this, &param) < 0;
                }

                int Cmp(const GUID* p1, const GUID* p2)
                {
                    return memcmp(p1, p2, sizeof(*p1));
                }

                bool IsEqual(const GUID* p1, const GUID* p2)
                {
                    return Cmp(p1, p2) == 0;
                }

                std::string ToString(const GUID* g)
                {
                    if (g == NULL)
                    {
                        return "";
                    }

                    char szBuffer[128] = "";
                    snprintf(szBuffer, 63, "{%08X-%04X-%04X-%02X%02X-%02X%02X%02X%02X%02X%02X}",
                        g->Data1, g->Data2, g->Data3,
                        g->Data4[0] & 0xFF, g->Data4[1] & 0xFF,
                        g->Data4[2] & 0xFF, g->Data4[3] & 0xFF, g->Data4[4] & 0xFF,
                        g->Data4[5] & 0xFF, g->Data4[6] & 0xFF, g->Data4[7] & 0xFF);

                    return szBuffer;
                }
            }

        }
    }
}

#ifdef _MAIN_TEST_
namespace zoo
{
    namespace common
    {
        namespace structure
        {
            namespace guid
            {
                namespace test
                {
                    void MainTest()
                    {
                        DEFINE_GUID(Unknow1, 100, 10, 20, 1, 2, 3, 4, 5, 6, 7, 8);
                        DEFINE_GUID(Unknow2, 100, 10, 20, 1, 2, 3, 4, 5, 6, 7, 8);

                        bool b = zoo::common::structure::guid::IsEqual(&Unknow1, &Unknow2);
                        global::_sLog.WriteLog(std::string("guid is equal: ").append(std::to_string(b)).c_str());;

                        int n = zoo::common::structure::guid::Cmp(&Unknow1, &Unknow2);
                        global::_sLog.WriteLog(std::string("guid cmp     : ").append(std::to_string(n)).c_str());;

                        global::_sLog.WriteLog(zoo::common::structure::guid::ToString(&Unknow1).c_str());
                        global::_sLog.WriteLog(zoo::common::structure::guid::ToString(&Unknow2).c_str());

                        int k = memcmp(&Unknow1, &Unknow2, sizeof(Unknow1));
                        global::_sLog.WriteLog(std::string("guid cmp 2   : ").append(std::to_string(k)).c_str());;
                        global::_sLog.WriteLog(std::string("guid len     : ").append(std::to_string(sizeof(Unknow1))).c_str());;


                        auto str1 = zoo::common::memory::Print(&Unknow1, sizeof(Unknow1));
                        global::_sLog.WriteLog(std::string("\nguid u 1     : \n").append(str1).c_str());

                        auto str2 = zoo::common::memory::Print(&Unknow2, sizeof(Unknow2));
                        global::_sLog.WriteLog(std::string("\nguid u 2     : \n").append(str2).c_str());
                    }
                }
            }
        }
    }
}
#endif  //_MAIN_TEST_

