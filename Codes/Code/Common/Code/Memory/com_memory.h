/**
 * @file com_memory.h
 * @author sqlxss (276793422@qq.com)
 * @brief
 * @version 0.1
 * @date 2023-04-23
 *
 * Copyright (c) 2023 sqlxss(zhaojianzu)
 *
 */
#include "com_base.h"
#pragma once

namespace zoo
{
    namespace common
    {
        namespace memory
        {
            void *Alloc(long n);

            void Free(void* p);

            void *AllocWithTag(long n, long nTag);

            void FreeWithTag(void* p, long nTag);

            int32_t Cmp(const void* pBuf1, const void* pBuf2, uint32_t nLen);

            std::string Print(const void* p, long nLen);
        }
    }
}




