#include "com_memory.h"
#include <stdlib.h>
#include <string.h>

namespace zoo
{
    namespace common
    {
        namespace memory
        {
            void *Alloc(long n)
            {
                return malloc(n);
            }

            void Free(void* p)
            {
                free(p);
            }

            /// @brief tag 函数是新增加的，用来标记内存申请源，供后续调试使用
            /// @param n
            /// @param nTag 内存申请标记，用来标记内存申请源
            /// @return
            void *AllocWithTag(long n, long nTag)
            {
                return malloc(n);
            }

            void FreeWithTag(void* p, long nTag)
            {
                return free(p);
            }

            int32_t Cmp(const void* pBuf1, const void* pBuf2, uint32_t nLen)
            {
                return memcmp(pBuf1, pBuf2, nLen);
            }

            std::string Print(const void* p, long nLen)
            {
                if (p == NULL || nLen == 0)
                {
                    return "";
                }
                std::string strRet = "         00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F";
                char szBuffer[16] = "";
                const char* sz = (const char*)p;
                for (long n = 0; n < nLen; n++)
                {
                    if ((n & 0xF) == 0)
                    {
                        snprintf(szBuffer, 15, "%08X ", (unsigned int)(n & ~0xF));
                        strRet.append("\n").append(szBuffer);
                    }
                    snprintf(szBuffer, 15, "%02X ", sz[n]);
                    strRet.append(szBuffer);
                }
                return strRet;
            }
        }
    }
}

