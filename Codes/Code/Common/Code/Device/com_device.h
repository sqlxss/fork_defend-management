/**
 * @file com_device.h
 * @author sqlxss (276793422@qq.com)
 * @brief
 * @version 0.1
 * @date 2023-04-23
 *
 * Copyright (c) 2023 sqlxss(zhaojianzu)
 *
 */
#include "com_base.h"
#pragma once

namespace zoo
{
    namespace common
    {
        namespace device
        {
            /**
             * @brief 当前类提供设备打开，并且交互相关功能，
             *        当前类提供设备读写相关功能，可以做设备直接交互。
             */
            class CDevice
            {
            public:
                CDevice();
                ~CDevice();

                /**
                 * @brief Construct a new CDevice object
                 *
                 * @param szPath 设备路径
                 * @param nFlag 设备打开标记，默认为2,值为O_RDWR
                 */
                CDevice(const char* szPath, int nFlag=2);   // O_RDWR

                /**
                 * @brief 对象初始化功能
                 *
                 * @param szPath 设备路径
                 * @param nFlag 设备打开标记，默认为2,值为O_RDWR
                 * @return true 打开设备成功
                 * @return false 打开设备失败
                 */
                bool Init(const char* szPath, int nFlag=2); // O_RDWR

                /**
                 * @brief 判断当前对象是否已经打开设备，是否准备就绪
                 *
                 * @return true 是
                 * @return false 否，需要重新打开设备
                 */
                bool IsReady();

                /**
                 * @brief 从设备中读取一块数据
                 *
                 * @param szBuffer 读取数据存放的缓冲区
                 * @param nSize 读取数据缓冲区长度
                 * @return long 读取的字符长度
                 */
                long Read(void* szBuffer, size_t nSize);

                /**
                 * @brief 向设备写入一块数据
                 *
                 * @param szBuffer 存放数据的缓冲区
                 * @param nSize 存放数据的缓冲区长度
                 * @return long 写入的数据数量
                 */
                long Write(const void* szBuffer, size_t nSize);

                /**
                 * @brief 关闭对象链接
                 *        关闭之后，对象将不可用，若要使用，则需要重新Init
                 * @return true
                 * @return false
                 */
                bool Close();
            private:

                int _fd;

                bool _bInited;
            };
        }
    }
}


#ifdef _MAIN_TEST_
namespace zoo
{
    namespace common
    {
        namespace device
        {
            namespace test
            {
                void MainTest();
            }
        }
    }
}
#endif  //_MAIN_TEST_



