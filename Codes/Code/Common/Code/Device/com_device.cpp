#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include "com_device.h"


namespace zoo
{
    namespace common
    {
        namespace device
        {
            CDevice::CDevice()
            {
                _fd = 0;
                _bInited = false;
            }

            CDevice::~CDevice()
            {
                Close();
            }

            CDevice::CDevice(const char* szPath, int nFlag)
            {
                _fd = 0;
                _bInited = false;
                Init(szPath, nFlag);
            }

            bool CDevice::Init(const char* szPath, int nFlag)
            {
                if (_fd != 0)
                {
                    return true;
                }

                _fd = ::open(szPath, nFlag);

                _bInited = (_fd != 0);

                return _bInited;
            }

            bool CDevice::IsReady()
            {
                return _bInited;
            }


            long CDevice::Read(void* szBuffer, size_t nSize)
            {
                if (_fd == 0)
                {
                    return 0;
                }
                return (long)read(_fd, szBuffer, nSize);
            }

            long CDevice::Write(const void* szBuffer, size_t nSize)
            {
                if (_fd == 0)
                {
                    return 0;
                }
                return (long)write(_fd, szBuffer, nSize);
            }

            bool CDevice::Close()
            {
                if (_fd != 0)
                {
                    ::close(_fd);
                    _fd = 0;
                }
                _bInited = false;
                return true;
            }
        }
    }
}

#ifdef _MAIN_TEST_
#include "../Memory/com_memory.h"
namespace zoo
{
    namespace common
    {
        namespace device
        {
            namespace test
            {
                void MainTest()
                {
                    char *p = (char*)zoo::common::memory::Alloc(128);

                    int nRet = 0;
                    zoo::common::device::CDevice dev("/dev/defender_sqlxss", O_RDWR);
                    
                    if (dev.IsReady() == false)
                    {
                        printf("open device error \n");
                    }
                    else
                    {
                        printf("open device success\n");

                        nRet = dev.Read(p, 128);
                        printf("read  = %s\n", p);
                        nRet = dev.Write("1234", 5);
                        printf("write = \n");

                        dev.Close();
                    }

                    zoo::common::memory::Free(p);
                }
            }
        }
    }
}
#endif  //_MAIN_TEST_




