/**
 * @file com_module.h
 * @author sqlxss (276793422@qq.com)
 * @brief
 * @version 0.1
 * @date 2023-07-04
 *
 * Copyright (c) 2023 sqlxss(zhaojianzu)
 *
 */
#include "com_base.h"
#pragma once

namespace zoo
{
    namespace common
    {
        namespace module
        {
            class CLibrary
            {
            public:
                CLibrary();
                ~CLibrary();

                lerror Load(const char* lpcsPath);

                lerror GetModule(const char* lpcsPath);

                lerror GetProc(const char* lpcsName, void* *f);

                lerror Free();

            private:
                void* _h;
            };


            void *Load(const char* lpcsPath);

            void *GetModule(const char* lpcsPath);

            void *LoadFlag(const char* lpcsPath, int nFlag);

            void *GetProcAddress(void *h, const char* lpcsName);

            lerror Free(void* h);
        }
    }
}


