#include "com_module.h"


namespace zoo
{
    namespace common
    {
        namespace module
        {

            CLibrary::CLibrary()
            {
                _h = NULL;
            }

            CLibrary::~CLibrary()
            {
                if (_h != NULL)
                {
                    zoo::common::module::Free(_h);
                }
            }

            lerror CLibrary::Load(const char* lpcsPath)
            {
                _h = zoo::common::module::Load(lpcsPath);
                if (_h == NULL)
                {
                    return 1;
                }
                return 0;
            }

            lerror CLibrary::GetModule(const char* lpcsPath)
            {
                _h = zoo::common::module::GetModule(lpcsPath);
                if (_h == NULL)
                {
                    return 1;
                }
                return 0;
            }

            lerror CLibrary::GetProc(const char* lpcsName, void* *f)
            {
                *f = zoo::common::module::GetProcAddress(_h, lpcsName);
                if (*f == NULL)
                {
                    return 1;
                }
                return 0;
            }

            lerror CLibrary::Free()
            {
                uint32_t nRet = zoo::common::module::Free(_h);
                if (nRet == 0)
                {
                    _h = NULL;
                }
                return nRet;
            }



            void *Load(const char* lpcsPath)
            {
                return LoadFlag(lpcsPath, RTLD_NOW);
            }

            void *GetModule(const char* lpcsPath)
            {
                return NULL;
            }

            void *LoadFlag(const char* lpcsPath, int nFlag)
            {
                try
                {
                    void* h = dlopen(lpcsPath, nFlag);

                    return h;
                }
                catch(...)
                {
                    return NULL;
                }
                return NULL;
            }

            void *GetProcAddress(void *h, const char* lpcsName)
            {
                try
                {
                    void* f = dlsym(h, lpcsName);
                    return f;
                }
                catch(...)
                {
                }
                return NULL;
            }

            lerror Free(void* h)
            {
                try
                {
                    uint32_t nRet = dlclose(h);
                    return nRet;
                }
                catch(...)
                {
                }
                return -1;
            }
        }
    }
}
