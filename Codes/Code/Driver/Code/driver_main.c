
#include "Core/common/header.h"
#include "Core/common/common.h"

static int defender_open(struct inode *pInode, struct file *pFile)
{
	zoo_com_log_WriteLog(NULL, "defender_open success\n");
	return 0;
}

static ssize_t defender_read(struct file *pFile, char __user *pBuf, size_t cbSize, loff_t *pPos)
{
	int nRet;
	zoo_com_log_WriteLog(NULL, "defender_read success\n");
	nRet = copy_to_user(pBuf, "1111", 5);
	zoo_com_log_WriteLog(NULL, "\tdefender_read = %d", nRet);
	return 0;
}

static ssize_t defender_write(struct file *pFile, const char __user *pBuf, size_t cbSize, loff_t *pPos)
{
	char szBuffer[128];
	int nRet;
	zoo_com_log_WriteLog(NULL, "defender_write success\n");
	nRet = copy_from_user(szBuffer, pBuf, cbSize);
	szBuffer[cbSize] = '\0';
	zoo_com_log_WriteLog(NULL, "\tdefender_write = %d : %s\n", nRet, szBuffer);
	return 0;
}

static struct file_operations _defender_fops = {
	.owner = THIS_MODULE,
	.open = defender_open,
	.read = defender_read,
	.write = defender_write,
};


static zoo_com_device_DEVICE_OBJECT dev;

static int _defender_major = 231;						//  主设备ID
static int _defender_minor = 0;							//  子设备ID
static char *_defender_module_name = "defender_sqlxss"; //  模块名

int __init defender_init(void)
{
	lerror e = 0;

	zoo_com_memory_Init();

	zoo_com_log_WriteLog(NULL, "defender_init success\n");

	e = zoo_com_device_Create(&dev, _defender_major, _defender_minor, _defender_module_name, THIS_MODULE, &_defender_fops);

	zoo_com_global_GlobalInit();

	zoo_com_global_PrintGlobal();

	zoo_com_log_WriteLog(NULL, "get func : security_add_hooks : 0x%lX", (long unsigned int)zoo_com_global_GetFunctionAddress("security_add_hooks"));

	zoo_com_log_WriteLog(NULL, "create device : %ld\n", e);

	zoo_com_log_WriteLog(NULL, "Init Global Value\n");

	zoo_com_callback_lsm_Init();

	zoo_com_callback_lsm_Start();

/*
	{
		zoo_com_memory_PrintKernel(zoo_com_global_GetFunctionAddress("kallsyms_lookup_name"), 128);
		zoo_com_memory_PrintKernel(zoo_com_global_GetFunctionAddress("security_add_hooks"), 128);
		zoo_com_memory_PrintKernel(zoo_com_global_GetFunctionAddress("security_hook_heads"), 128);
	}
*/

	return 0;
}

void __exit defender_exit(void)
{
	lerror e = 0;

	//zoo_com_callback_lsm_Stop();

	//zoo_com_callback_lsm_Destroy();

	e = zoo_com_device_stop(&dev);

	zoo_com_log_WriteLog(NULL, "delete device : %ld\n", e);

	zoo_com_log_WriteLog(NULL, "defender_exit success\n");
}

module_init(defender_init);
module_exit(defender_exit);
MODULE_LICENSE("GPL v2");
