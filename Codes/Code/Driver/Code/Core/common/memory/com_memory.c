#include "com_memory.h"


void zoo_com_memory_Init(void)
{

}

void* zoo_com_memory_vmem_Alloc(size_t nCount, gfp_t flag, long nTag)
{
	if (flag == 0)
	{
		flag = GFP_KERNEL;
	}
	return kmalloc(nCount, flag);
}

void* zoo_com_memory_vmem_ZAlloc(size_t nCount, gfp_t flag, long nTag)
{
	if (flag == 0)
	{
		flag = GFP_KERNEL;
	}
	return kzalloc(nCount, flag);
}

void zoo_com_memory_vmem_Free(void* p, long nTag)
{
	kfree(p);
}

void zoo_com_memory_Zero(void* p, size_t nCount)
{
	memset(p, 0, nCount);
}


void zoo_com_memory_PrintKernel(const void* p, long nLen)
{
	char szLineBuffer[128] = "";
	char szBuffer[16] = "";
	const char* sz = (const char*)p;
	long n;
	if (p == NULL || nLen == 0)
	{
		return ;
	}
	printk("Address : 0x%lX", (unsigned long int)p);
	printk("         00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F\n");
	for (n = 0; n < nLen; n++)
	{
		if ((n & 0xF) == 0)
		{
			if (szLineBuffer[0] != '\0')
			{
				printk("%s\n", szLineBuffer);
				szLineBuffer[0] = '\0';
			}
			snprintf(szLineBuffer, 15, "%08X ", (unsigned int)(n & ~0xF));
		}
		snprintf(szBuffer, 15, "%02X ", sz[n] & 0xFF);
		strcat(szLineBuffer, szBuffer);
	}
	printk("%s\n", szLineBuffer);
	return ;
}
