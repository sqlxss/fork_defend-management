/**
 * @file com_memory.h
 * @author sqlxss (276793422@qq.com)
 * @brief
 * @version 0.1
 * @date 2023-07-27
 *
 * Copyright (c) 2023 sqlxss (zhaojianzu)
 *
 */

#include "../header.h"

#pragma once

//	zoo_com_memory

/**
 * @brief 内存功能初始化
 *
 */
void zoo_com_memory_Init(void);

/**
 * @brief 带 TAG 的虚拟内存申请
 *
 * @param nCount
 * @param flag 如果值是0，默认是  GFP_KERNEL
 * @param nTag
 */
void* zoo_com_memory_vmem_Alloc(size_t nCount, gfp_t flag, long nTag);

/**
 * @brief 带 TAG 的虚拟内存申请
 *
 * @param nCount
 * @param flag 如果值是0，默认是  GFP_KERNEL
 * @param nTag
 */
void* zoo_com_memory_vmem_ZAlloc(size_t nCount, gfp_t flag, long nTag);

/**
 * @brief 带 TAG 的虚拟内存释放
 *
 * @param p
 * @param nTag
 */
void zoo_com_memory_vmem_Free(void* p, long nTag);

/**
 * @brief 内存清零
 *
 * @param p
 * @param nCOunt
 */
void zoo_com_memory_Zero(void* p, size_t nCount);

/**
 * @brief
 *
 * @param p
 * @param nLen
 */
void zoo_com_memory_PrintKernel(const void* p, long nLen);











