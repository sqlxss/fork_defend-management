/**
 * @file com_global.h
 * @author sqlxss (276793422@qq.com)
 * @brief
 * @version 0.1
 * @date 2023-07-05
 *
 * Copyright (c) 2023 sqlxss(zhaojianzu)
 *
 */
#include "../header.h"

/**
 * @brief 	这个文件提供了一些系统函数的支持，这些支持包括一些系统未导出的函数，
 *			但是要求，使用前先来这里定义，这一点比较坑，
 */

#pragma once

//	zoo_com_global


#define FCALL(__X)		(typeof(__X)*)zoo_com_global_GetFunctionAddress(#__X)

/**
 * @brief 基础方案，通过调试信息寻找对应符号
 *
 * @return lerror
 */
lerror zoo_com_global_GlobalInit(void);

lerror zoo_com_global_GlobalInit1(void* fp_kallsyms_lookup_name);

lerror zoo_com_global_GlobalInit2(void *fp_kallsyms_on_each_symbol);

lerror zoo_com_global_GlobalDestroy(void);

lerror zoo_com_global_PrintGlobal(void);

/**
 * @brief 根据函数名，找到对应的函数地址
 *
 * @param szFuncName 传入函数名
 * @return void*
 */
void* zoo_com_global_GetFunctionAddress(const char* szFuncName);














