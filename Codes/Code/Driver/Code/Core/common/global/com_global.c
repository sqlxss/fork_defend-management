#include "../header.h"
#include "com_global.h"

/**
 * @brief 这笔改动之后，结构不再暴露，
 * 			后续如果需要找函数，就只能直接找了
 */

typedef struct _zoo_com_global_FUNCTION_LIST_NODE
{
	const char *szFuncName;
	void* fp;
}zoo_com_global_FUNCTION_LIST_NODE;

#define __ADD_FUNCTION_LIST_NODE(__X)			{ .szFuncName = #__X , .fp = NULL }

/**
 * @brief 这里目前的缺陷是，如果想要增加函数，需要手动来增加，不能动态添加
 * 			后续把这里的结构换成动态结构，就可以了
 */

zoo_com_global_FUNCTION_LIST_NODE g_sFunctionArray[] =
{
	__ADD_FUNCTION_LIST_NODE(kallsyms_lookup_name),
	__ADD_FUNCTION_LIST_NODE(security_add_hooks),
	__ADD_FUNCTION_LIST_NODE(security_hook_heads),
};
int g_nFunctionCount = sizeof(g_sFunctionArray)/sizeof(g_sFunctionArray[0]);

////////////////////////////////////////////////////////////////////////////////////////////////////

lerror zoo_com_global_GlobalInit1(void *fp_kallsyms_lookup_name)
{
	int i = 0;
	zoo_com_global_FUNCTION_LIST_NODE *pNode = g_sFunctionArray;
	typeof(kallsyms_lookup_name) *fp = (typeof(kallsyms_lookup_name) *)fp_kallsyms_lookup_name;
	printk("%30s : 0x%lX\n", "kallsyms_lookup_name", (long unsigned int)fp_kallsyms_lookup_name);

	for (i = 0; i < g_nFunctionCount; i++)
	{
		if (pNode[i].szFuncName)
		{
			pNode[i].fp = (void *)fp(pNode[i].szFuncName);
		}
	}

	return 0;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

static int FuncCallback(void *data, const char *name, struct module *mod, unsigned long addr)
{
	if (strcmp("kallsyms_lookup_name", name) == 0)
	{
		void* *p = (void **)data;
		*p = (void*) addr;
		printk("Find : %30s : 0x%lX\n", "kallsyms_lookup_name", (long unsigned int)addr);
	}
	return 0;
}

lerror zoo_com_global_GlobalInit2(void *fp_kallsyms_on_each_symbol)
{
	void *fp_kallsyms_lookup_name = NULL;
	typeof(kallsyms_on_each_symbol) *fp = (typeof(kallsyms_on_each_symbol) *)fp_kallsyms_on_each_symbol;
	int r = 0;
	if (fp == NULL)
	{
		return 1;
	}
	r = fp(FuncCallback, &fp_kallsyms_lookup_name);
	if(r != 0)
	{
		return (lerror)r;
	}
	if (fp_kallsyms_lookup_name == NULL)
	{
		return 1;
	}
	return zoo_com_global_GlobalInit1(fp_kallsyms_lookup_name);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

static int p_tmp_kprobe_handler(struct kprobe* p, struct pt_regs *pReg)
{
	return 0;
}

lerror zoo_com_global_GlobalInit(void)
{
	struct kprobe _kprobe;
	int nRet = 0;
	lerror e = 0;

	memset(&_kprobe, 0, sizeof(struct kprobe));
	_kprobe.pre_handler = p_tmp_kprobe_handler;
	_kprobe.symbol_name = "kallsyms_lookup_name";

	nRet = register_kprobe(&_kprobe);
	if (nRet < 0)
	{
		return 1;
	}
	e = zoo_com_global_GlobalInit1(_kprobe.addr);

	unregister_kprobe(&_kprobe);

	return e;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

lerror zoo_com_global_GlobalDestroy(void)
{
	return 0;
}

lerror zoo_com_global_PrintGlobal(void)
{
	int i = 0;
	printk("func count : %d \n", g_nFunctionCount);
	for (i = 0; i < g_nFunctionCount; i++)
	{
		if (g_sFunctionArray[i].szFuncName == NULL)
		{
			break;
		}
		printk("List %30s : 0x%lX\n", g_sFunctionArray[i].szFuncName, (long unsigned int)g_sFunctionArray[i].fp);
	}
	return 0;
}

void* zoo_com_global_GetFunctionAddress(const char* szFuncName)
{
	int i = 0;
	for (i = 0; i < g_nFunctionCount; i++)
	{
		if (g_sFunctionArray[i].szFuncName == NULL)
		{
			break;
		}
		if (strcmp(g_sFunctionArray[i].szFuncName, szFuncName) == 0)
		{
			return g_sFunctionArray[i].fp;
		}
	}
	return NULL;
}
