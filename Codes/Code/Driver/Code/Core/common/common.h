/**
 * @file common.h
 * @author sqlxss (276793422@qq.com)
 * @brief
 * @version 0.1
 * @date 2023-07-06
 *
 * Copyright (c) 2023 sqlxss (zhaojianzu)
 *
 */

#pragma once

#include "header.h"

#include "memory/com_memory.h"

//	全局 log 部分
#include "log/com_log.h"

//	全局变量初始化部分
#include "global/com_global.h"

//
#include "device/com_device.h"

//
#include "callback/com_callback_header.h"
