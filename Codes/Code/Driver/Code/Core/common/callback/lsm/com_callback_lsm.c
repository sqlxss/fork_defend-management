#include <linux/security.h>
#include <linux/module.h>
#include <linux/lsm_hooks.h>

#include "../../header.h"
#include "../../common.h"
#include "com_callback_lsm.h"
#include "com_callback_lsm_core.h"



static int defender_task_alloc(struct task_struct *task, unsigned long clone_flags)
{
	printk("call defender_task_alloc \n");
	return 0;
}

static void defender_task_free(struct task_struct *task)
{
	printk("call defender_task_free \n");
}

static int defender_task_kill(struct task_struct *target, struct kernel_siginfo *info, int sig, const struct cred *cred)
{
	printk("call defender_task_kill \n");
	return 0;
}

static struct security_hook_heads* _security_hook_heads = NULL;

#define __LSM_HOOK_INIT(HEAD, HOOK) \
	{ .head = &(((struct security_hook_heads*)0)->HEAD), .hook = { .HEAD = HOOK } }

static struct security_hook_list defender_hooks[] = {
	__LSM_HOOK_INIT(task_alloc, defender_task_alloc),
	__LSM_HOOK_INIT(task_free, defender_task_free),
	__LSM_HOOK_INIT(task_kill, defender_task_kill),
};

static struct lsm_id defender_id = {
	"defender"
};

static BOOL _bInit = FALSE;

//
lerror zoo_com_callback_lsm_Init(void)
{
	int i = 0;

	if (_security_hook_heads == NULL)
	{
		_security_hook_heads = zoo_com_global_GetFunctionAddress("security_hook_heads");
	}
	if (_security_hook_heads == NULL)
	{
		return 1;
	}
	_bInit = TRUE;

	//	修复所有调用点
	for (i = 0; i < sizeof(defender_hooks)/sizeof(defender_hooks[0]); i++)
	{
		defender_hooks[i].head = (void *)defender_hooks[i].head + (long)_security_hook_heads;
	}
	return zoo_com_callback_lsm_core_Init();
}

//
lerror zoo_com_callback_lsm_Start(void)
{
	if (_bInit == FALSE)
	{
		return 1;
	}
	return zoo_com_callback_lsm_core_Start(defender_hooks, ARRAY_SIZE(defender_hooks), &defender_id);
}

lerror zoo_com_callback_lsm_Stop(void)
{
	if (_bInit == FALSE)
	{
		return 1;
	}
	return zoo_com_callback_lsm_core_Stop(defender_hooks, ARRAY_SIZE(defender_hooks));
}

lerror zoo_com_callback_lsm_Destroy(void)
{
	if (_bInit == FALSE)
	{
		return 1;
	}
	return zoo_com_callback_lsm_core_Destroy();
}
