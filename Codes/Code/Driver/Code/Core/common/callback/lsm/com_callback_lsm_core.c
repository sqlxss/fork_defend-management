#include "../../header.h"
#include "../../common.h"

#include <linux/security.h>
#include <linux/module.h>
#include <linux/lsm_hooks.h>

#include "com_callback_lsm_core.h"




lerror zoo_com_callback_lsm_core_Init(void)
{
	return 0;
}

lerror zoo_com_callback_lsm_core_Start(struct security_hook_list *hooks, int count, struct lsm_id* id)
{
	typeof(security_add_hooks)* fp = zoo_com_global_GetFunctionAddress("security_add_hooks");
	fp(hooks, count, id);
	return 0;
}

lerror zoo_com_callback_lsm_core_Stop(struct security_hook_list *hooks, int count)
{
	int i;
	for (i = 0; i < count; i++)
	{
		hlist_del_rcu(&hooks[i].list);
	}
	return 0;
}

lerror zoo_com_callback_lsm_core_Destroy(void)
{
	return 0;
}














