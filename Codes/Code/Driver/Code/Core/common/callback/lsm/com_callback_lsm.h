/**
 * @file com_callback_lsm.h
 * @author sqlxss (276793422@qq.com)
 * @brief
 * @version 0.1
 * @date 2023-07-29
 *
 * Copyright (c) 2023 sqlxss (zhaojianzu)
 *
 */

#include "../../header.h"

#pragma once

//	zoo_com_callback_lsm



lerror zoo_com_callback_lsm_Init(void);

lerror zoo_com_callback_lsm_Start(void);

lerror zoo_com_callback_lsm_Stop(void);

lerror zoo_com_callback_lsm_Destroy(void);

