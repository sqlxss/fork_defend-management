/**
 * @file com_callback_lsm_core.h
 * @author sqlxss (276793422@qq.com)
 * @brief
 * @version 0.1
 * @date 2023-07-29
 *
 * Copyright (c) 2023 sqlxss (zhaojianzu)
 *
 */

#include <linux/security.h>
#include <linux/module.h>
#include <linux/lsm_hooks.h>

#pragma once

//	zoo_com_callback_lsm_core



lerror zoo_com_callback_lsm_core_Init(void);

lerror zoo_com_callback_lsm_core_Start(struct security_hook_list *hooks, int count, struct lsm_id* id);

lerror zoo_com_callback_lsm_core_Stop(struct security_hook_list *hooks, int count);

lerror zoo_com_callback_lsm_core_Destroy(void);



