#include "../header.h"
#include "../common.h"
#include "com_device.h"




lerror zoo_com_device_Init(zoo_com_device_DEVICE_OBJECT* pThis, int major, int minor, const char* name)
{
	pThis->_defender_major = major;
	pThis->_defender_minor = minor;
	pThis->_defender_module_name = name;

	return 0;
}

lerror zoo_com_device_SetModuleCallbacks(zoo_com_device_DEVICE_OBJECT* pThis, struct module* mod, struct file_operations* op)
{
	pThis->_defender_module_object = mod;
	pThis->_defender_fops = op;

	return 0;
}

lerror zoo_com_device_Start(zoo_com_device_DEVICE_OBJECT* pThis)
{
	int ret = 0;

	if (pThis->_defender_module_name == NULL)
	{
		return 1;
	}

	if (pThis->_defender_fops == NULL)
	{
		return 1;
	}

	pThis->_defender_devno = MKDEV(pThis->_defender_major, pThis->_defender_minor);

	//	这里不会失败

	ret = register_chrdev(pThis->_defender_major, pThis->_defender_module_name, pThis->_defender_fops);

	if (ret != 0)
	{
		//	返回非 0 这里失败
		return ret;
	}

	pThis->_defender_class = class_create(pThis->_defender_module_object, pThis->_defender_module_name);

	if (pThis->_defender_class == NULL)
	{
		//	创建失败
		return 1;
	}

	pThis->_defender_class_dev = device_create(pThis->_defender_class, NULL, pThis->_defender_devno, NULL, pThis->_defender_module_name);

	if (pThis->_defender_class_dev == NULL)
	{
		//	创建失败
		return 1;
	}

	return 0;
}

lerror zoo_com_device_stop(zoo_com_device_DEVICE_OBJECT* pThis)
{
	device_destroy(pThis->_defender_class, pThis->_defender_devno);

	class_destroy(pThis->_defender_class);

	unregister_chrdev(pThis->_defender_major, pThis->_defender_module_name);

	zoo_com_memory_Zero(pThis, sizeof(zoo_com_device_DEVICE_OBJECT));

	//	这函数全程没有返回值

	return 0;
}

lerror zoo_com_device_Create(zoo_com_device_DEVICE_OBJECT* pThis, int major, int minor, const char* name, struct module* mod, struct file_operations* op)
{
	lerror e = 0;

	e = zoo_com_device_Init(pThis, major, minor, name);
	if (e != 0)
	{
		return e;
	}

	e = zoo_com_device_SetModuleCallbacks(pThis, mod, op);
	if (e != 0)
	{
		return e;
	}

	e = zoo_com_device_Start(pThis);

	return e;
}

zoo_com_device_DEVICE_OBJECT* zoo_com_device_Create2(int major, int minor, const char* name, struct module* mod, struct file_operations* op)
{
	lerror e = 0;

	zoo_com_device_DEVICE_OBJECT* pThis = (zoo_com_device_DEVICE_OBJECT*)zoo_com_memory_vmem_ZAlloc(sizeof(zoo_com_device_DEVICE_OBJECT), 0, 0);

	if (pThis == NULL)
	{
		return NULL;
	}

	e = zoo_com_device_Create(pThis, major, minor, name, mod, op);

	if (e != 0)
	{
		zoo_com_memory_vmem_Free(pThis, 0);

		return NULL;
	}

	return pThis;
}


