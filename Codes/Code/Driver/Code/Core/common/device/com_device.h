/**
 * @file com_device.h
 * @author sqlxss (276793422@qq.com)
 * @brief
 * @version 0.1
 * @date 2023-07-27
 *
 * Copyright (c) 2023 sqlxss (zhaojianzu)
 *
 */

#include "../header.h"
#pragma once

//	zoo_com_device

typedef struct _zoo_com_device_DEVICE_OBJECT
{
	struct class *_defender_class;						//	设备类
	struct device *_defender_class_dev;					//	设备对象

	dev_t _defender_devno;								//  设备号
	int _defender_major;								//  主设备ID
	int _defender_minor;								//  子设备ID
	const char *_defender_module_name;					//  模块名

	struct module* _defender_module_object;				//	归属模块指针
	struct file_operations* _defender_fops;				//	回调函数列表
}zoo_com_device_DEVICE_OBJECT;


/**
 * @brief 	初始化一个设备对象结构
 *
 * @param pThis
 * @param major
 * @param minor
 * @param name
 * @return lerror
 */
lerror zoo_com_device_Init(zoo_com_device_DEVICE_OBJECT* pThis, int major, int minor, const char* name);

/**
 * @brief 	给设备对象设置关键信息
 * 			实际上，如果不设置的话，这设备对象也用不了
 * @param pThis
 * @param mod
 * @param op
 * @return lerror
 */
lerror zoo_com_device_SetModuleCallbacks(zoo_com_device_DEVICE_OBJECT* pThis, struct module* mod, struct file_operations* op);

/**
 * @brief	启动当前设备
 *
 * @param pThis
 * @return lerror
 */
lerror zoo_com_device_Start(zoo_com_device_DEVICE_OBJECT* pThis);

/**
 * @brief 	停止当前设备
 * 			内部的功能实际上是删除这个设备，不只是停止，内部内容也会清空
 * @param pThis
 * @return lerror
 */
lerror zoo_com_device_stop(zoo_com_device_DEVICE_OBJECT* pThis);

/**
 * @brief 	直接创建一个新的设备
 *
 * @param pThis
 * @param major
 * @param minor
 * @param name
 * @param mod
 * @param op
 * @return lerror
 */
lerror zoo_com_device_Create(zoo_com_device_DEVICE_OBJECT* pThis, int major, int minor, const char* name, struct module* mod, struct file_operations* op);

/**
 * @brief 	直接创建一个新的设备，
 * 			内存由内部自己申请
 * @param major
 * @param minor
 * @param name
 * @param mod
 * @param op
 * @return zoo_com_device_DEVICE_OBJECT*
 */
zoo_com_device_DEVICE_OBJECT* zoo_com_device_Create2(int major, int minor, const char* name, struct module* mod, struct file_operations* op);







