/**
 * @file com_hash.h
 * @author sqlxss (276793422@qq.com)
 * @brief
 * @version 0.1
 * @date 2023-07-28
 *
 * Copyright (c) 2023 sqlxss (zhaojianzu)
 *
 */

#include "../header.h"
#pragma once

//	zoo_com_list


typedef struct _zoo_com_list_LIST_HEAD
{
    struct list_head head;
    spinlock_t lock;
}zoo_com_list_LIST_HEAD;


void zoo_com_list_Init(zoo_com_list_LIST_HEAD *pThis);


void zoo_com_list_Add(zoo_com_list_LIST_HEAD *pThis, struct list_head *data);


void zoo_com_list_Delete(zoo_com_list_LIST_HEAD *pThis, struct list_head *data);


lerror zoo_com_list_List_FunctionType_FT(struct list_head *pThis);
typedef typeof(zoo_com_list_List_FunctionType_FT)* zoo_com_list_List_FunctionType;

void zoo_com_list_Iterator(zoo_com_list_LIST_HEAD *pThis, zoo_com_list_List_FunctionType fp);

