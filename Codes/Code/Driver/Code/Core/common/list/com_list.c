#include "../header.h"

#include "com_list.h"



void zoo_com_list_Init(zoo_com_list_LIST_HEAD *pThis)
{
    spin_lock_init(&pThis->lock);
    INIT_LIST_HEAD(&pThis->head);
}


void zoo_com_list_Add(zoo_com_list_LIST_HEAD *pThis, struct list_head *data)
{
    spin_lock(&pThis->lock);
    list_add_tail(data, &pThis->head);
    spin_unlock(&pThis->lock);
}


void zoo_com_list_Delete(zoo_com_list_LIST_HEAD *pThis, struct list_head *data)
{
    spin_lock(&pThis->lock);
    list_del_init(data);
    spin_unlock(&pThis->lock);
}


void zoo_com_list_Iterator(zoo_com_list_LIST_HEAD *pThis, zoo_com_list_List_FunctionType fp)
{
	lerror e = 0;
    struct list_head *pos;
    struct list_head *n;
    spin_lock(&pThis->lock);
    list_for_each_safe(pos, n, &pThis->head)
	{
        e = fp(pos);
		if (e != 0)
		{
			break;
		}
    }
    spin_unlock(&pThis->lock);
}


typedef struct _zoo_com_list_LIST_NODE
{
    struct list_head head;
	void *pv;
}zoo_com_list_LIST_NODE;



