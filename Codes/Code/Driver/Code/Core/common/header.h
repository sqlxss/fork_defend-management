/**
 * @file header.h
 * @author sqlxss (276793422@qq.com)
 * @brief
 * @version 0.1
 * @date 2023-07-06
 *
 * Copyright (c) 2023 sqlxss (zhaojianzu)
 *
 */

#pragma once

#include <linux/module.h>
#include <linux/init.h>
#include <linux/device.h>
#include <linux/uaccess.h>
#include <linux/types.h>
#include <linux/kallsyms.h>
#include <linux/kernel.h>
#include <linux/sched.h>
#include <linux/kthread.h>
#include <linux/err.h>
#include <linux/delay.h>
#include <linux/gfp.h>
#include <linux/file.h>
#include <linux/fs.h>
#include <linux/init.h>
#include <linux/syscalls.h>
#include <linux/slab.h>
#include <linux/mm.h>
#include <linux/kprobes.h>
#include <linux/ptrace.h>
#include <asm/segment.h>
#include <asm/io.h>
#include <asm/uaccess.h>
#include <linux/lsm_hooks.h>



//#define lerror		long

typedef long lerror;


typedef int BOOL;

#define 	TRUE		1
#define 	FALSE		0















