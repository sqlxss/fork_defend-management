/**
 * @file com_log.h
 * @author sqlxss (276793422@qq.com)
 * @brief
 * @version 0.1
 * @date 2023-07-06
 *
 * Copyright (c) 2023 sqlxss (zhaojianzu)
 *
 */

/**
 * Log 部分，写日至会写两块，一块是 printk ，另一块是写 log 文件
 */

#include "../header.h"
#pragma once

//	zoo_com_log


typedef enum _zoo_com_log_ENUM_LOG_LEVEL
{
	ENUM_LOG_LEVEL_NONE = 0,
	ENUM_LOG_LEVEL_ERROR,
	ENUM_LOG_LEVEL_TRACE,
	ENUM_LOG_LEVEL_DEBUG,
	ENUM_LOG_LEVEL_INFO,
	ENUM_LOG_LEVEL_LOG,
	ENUM_LOG_LEVEL_MAX,
}zoo_com_log_ENUM_LOG_LEVEL;

typedef struct _zoo_com_log_LOG_OBJECT
{
	zoo_com_log_ENUM_LOG_LEVEL _eLevel;
	const char* _szPath;
	BOOL _bCreate;
	BOOL _bStart;
}zoo_com_log_LOG_OBJECT;

zoo_com_log_LOG_OBJECT* zoo_com_log_Create(zoo_com_log_ENUM_LOG_LEVEL eLevel, const char* szLogPath);

lerror zoo_com_log_Init(zoo_com_log_LOG_OBJECT* pThis);

lerror zoo_com_log_Start(zoo_com_log_LOG_OBJECT* pThis, zoo_com_log_ENUM_LOG_LEVEL eLevel, const char* szLogPath);



lerror zoo_com_log_Write(zoo_com_log_LOG_OBJECT* pThis, zoo_com_log_ENUM_LOG_LEVEL eLevel, const char* szLog, ...);

lerror zoo_com_log_WriteLog(zoo_com_log_LOG_OBJECT* pThis, const char* szLog, ...);



lerror zoo_com_log_Destroy(zoo_com_log_LOG_OBJECT* pThis);

lerror zoo_com_log_Delete(zoo_com_log_LOG_OBJECT* pThis);

