#include "../header.h"
#include "../common.h"

#include "com_log.h"


zoo_com_log_LOG_OBJECT* zoo_com_log_Create(zoo_com_log_ENUM_LOG_LEVEL eLevel, const char* szLogPath)
{
	lerror e = 0;
	zoo_com_log_LOG_OBJECT* pThis = NULL;
	BOOL bInit = FALSE;

	do
	{
		pThis = (zoo_com_log_LOG_OBJECT*)zoo_com_memory_vmem_ZAlloc(sizeof(zoo_com_log_LOG_OBJECT), 0, 0);
		if (pThis == NULL)
		{
			break;
		}

		e = zoo_com_log_Init(pThis);
		if (e != 0)
		{
			break;
		}

		e = zoo_com_log_Start(pThis, eLevel, szLogPath);
		if (e != 0)
		{
			break;
		}

		pThis->_bCreate = TRUE;
		bInit = TRUE;
	} while (FALSE);

	if (bInit == FALSE)
	{
		zoo_com_memory_vmem_Free(pThis, 0);
		pThis = NULL;
	}

	return pThis;
}

lerror zoo_com_log_Init(zoo_com_log_LOG_OBJECT* pThis)
{
	pThis->_bCreate = FALSE;
	pThis->_bStart = FALSE;
	pThis->_szPath = NULL;
	pThis->_eLevel = ENUM_LOG_LEVEL_NONE;
	return 0;
}

lerror zoo_com_log_Start(zoo_com_log_LOG_OBJECT* pThis, zoo_com_log_ENUM_LOG_LEVEL eLevel, const char* szLogPath)
{
	pThis->_bStart = TRUE;
	pThis->_szPath = szLogPath;
	pThis->_eLevel = eLevel;
	return 0;
}


lerror zoo_com_log_Write(zoo_com_log_LOG_OBJECT* pThis, zoo_com_log_ENUM_LOG_LEVEL eLevel, const char* szLog, ...)
{
	va_list args;
	va_start(args, szLog);
	vprintk(szLog, args);
	va_end(args);
	return 0;
}

lerror zoo_com_log_WriteLog(zoo_com_log_LOG_OBJECT* pThis, const char* szLog, ...)
{
	va_list args;
	if (pThis != NULL)
	{
		if (pThis->_eLevel < ENUM_LOG_LEVEL_LOG)
		{
			return 0;
		}
	}
	va_start(args, szLog);
	vprintk(szLog, args);
	va_end(args);
	return 0;
}


lerror zoo_com_log_Destroy(zoo_com_log_LOG_OBJECT* pThis)
{
	pThis->_bStart = FALSE;
	return 0;
}

lerror zoo_com_log_Delete(zoo_com_log_LOG_OBJECT* pThis)
{
	lerror e = 0;

	e = zoo_com_log_Destroy(pThis);
	if (e != 0)
	{
		return e;
	}

	if (pThis->_bCreate == TRUE)
	{
		zoo_com_memory_vmem_Free(pThis, 0);
	}
	return 0;
}
