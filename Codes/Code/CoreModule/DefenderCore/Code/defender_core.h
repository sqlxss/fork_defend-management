/**
 * @file defender_core.h
 * @author sqlxss (276793422@qq.com)
 * @brief
 * @version 0.1
 * @date 2023-07-25
 *
 * Copyright (c) 2023 sqlxss (zhaojianzu)
 *
 */

#include <string>
#include <vector>
#include "com_base.h"
#include "com_log.h"
#include "com_pool.h"

#pragma once



namespace zoo
{
	namespace defender
	{
		namespace core
		{
			class CDefender
			{
			public:
				CDefender();

				~CDefender();

				CDefender(void* pEnv);

			public:
				/**
				 * @brief 初始化函数
				 *			用途是初始化整个 class
				 * @param pEnv
				 * @return lerror
				 */
				lerror Init(void* pEnv);

				/**
				 * @brief 执行函数
				 * 			必须是先初始化然后才能执行
				 * 			这个函数是外部调用的执行函数，
				 * 			内部会判断是否可执行，然后调用 RunCore
				 * @return lerror
				 */
				lerror Run();

			private:
				/**
				 * @brief 执行函数
				 * 			这个是真实的执行函数，
				 * 			内部处理若干成员填充函数，最后调用 Loop 等待推出
				 * @return lerror
				 */
				lerror RunCore();

			private:
				int _bInit;

				//	全局日志系统
				zoo::common::log::CLog _sLog;

				//	全局对象池控制器，内部控制池管理器
				zoo::common::pool::CPoolControler _pool;

				//	全局对象当前路径，标记当前目录绝对路径
				std::string _strCurrentDir;

				//	全局对象模块列表，记录了当前系统存在的所有模块，包括可用和不可用的
				std::vector<std::string> _module;

				const char* _cfgFilePath;
			private:
				/**
				 * @brief 加载配置文件
				 * 			加载全部配置文件
				 * @return lerror
				 */
				lerror LoadConfig();

				/**
				 * @brief 加载池成员模块
				 * 			并且可以获取池成员接口，最后可供外部使用
				 * @return lerror
				 */
				lerror LoadPoolModule();

			private:
				/**
				 * @brief 加载池成员配置文件
				 * 			这个配置文件标记了所有需要加载的池成员模块，内部对应加载
				 */
				void LoadModuleListConfig();

			private:
				/**
				 * @brief 主模块的循环等待位置
				 * 			主要功能是防止进程退出，内部作为一个对象，等待退出信息
				 * @return lerror
				 */
				lerror RunLoop();
			};
		}
	}
}


