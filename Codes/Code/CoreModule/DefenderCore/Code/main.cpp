/**
 * @file main.cpp
 * @author sqlxss (276793422@qq.com)
 * @brief 此文件仅作为一个入口定义文件，其他什么都不做
 * 			内部定义了两个入口函数，具体看如下
 * @version 0.1
 * @date 2023-07-26
 *
 * Copyright (c) 2023 sqlxss (zhaojianzu)
 *
 */
#include "defender_core.h"



//	Plugin 入口
extern "C" int __(void* pEnv)
{
	return 0x00000F00;
}

//	Core 入口
extern "C" int _(void* pEnv)
{
	zoo::defender::core::CDefender defender(pEnv);
	return defender.Run();
}

