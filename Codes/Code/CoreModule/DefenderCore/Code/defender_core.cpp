#include "defender_core.h"
#include "com_base.h"
#include "com_file.h"
#include "com_structure.h"
#include "com_path.h"
#include "com_memory.h"
#include "com_string.h"
#include "error_code.interface.h"

namespace zoo
{
	namespace defender
	{
		namespace core
		{

			CDefender::CDefender()
			{
				_bInit = FALSE;
				_cfgFilePath = "module.cfg";
			}

			CDefender::~CDefender()
			{

			}

			CDefender::CDefender(void* pEnv) : CDefender()
			{
				Init(pEnv);
			}

			lerror CDefender::Init(void* pEnv)
			{
				if (_bInit == TRUE)
				{
					_sLog.WriteLog("defender core : init : reinit\n");
					return 0;
				}
				////
				//	1
				//		验证传入参数
				_sLog.WriteLog("step1 : confirm params : default success\n");

				////
				//	2
				//		初始化内部成员变量
				_sLog.WriteLog("step2 : init values\n");
				zoo::common::path::GetProcessFileDirReal(_strCurrentDir);
				_sLog.WriteLog(_strCurrentDir.c_str());

				////
				//	3
				//		初始化内部各种数据成员
				_sLog.WriteLog("step3 : init member\n");



				_bInit = TRUE;
				return 0;
			}

			lerror CDefender::Run()
			{
				if (_bInit == FALSE)
				{
					_sLog.WriteLog("defender core : init : uninit\n");
					return 0x00000100;
				}
				return RunCore();
			}

			lerror CDefender::RunCore()
			{
				lerror e = LoadConfig();
				if (e != 0)
				{
					return e;
				}

				e = LoadPoolModule();
				if (e != 0)
				{
					return e;
				}

				return RunLoop();
			}


			lerror CDefender::LoadConfig()
			{
				////
				//	4
				//		初始化加载各种所需的配置文件
				_sLog.WriteLog("step4 : init config\n");

				LoadModuleListConfig();

				return 0;
			}


			lerror CDefender::LoadPoolModule()
			{
				////
				//	5
				//		初始化池对象
				_sLog.WriteLog("step5 : init pool object\n");
				_pool.Init();

				////
				//	6
				//		初始化池功能，初始化池内成员
				_sLog.WriteLog("step6 : init pool member\n");
				for (auto it : _module)
				{
					std::string strPath = _strCurrentDir + zoo::common::string::RepalceString(it.c_str(), "\n", "");
					lerror e = zoo::common::pool::PoolObjectLoader(strPath.c_str(), &_pool);
					_sLog.WriteLog((strPath + std::string("\t") + std::to_string(e)).c_str());
				}

				////
				//	7
				//		全部初始化完成，做好数据收尾工作
				_sLog.WriteLog("step7 : init done\n");
				_pool.SetInited();

				return 0;
			}

			void CDefender::LoadModuleListConfig()
			{
				zoo::common::file::CFile cf;
				lerror e = cf.Open((_strCurrentDir + _cfgFilePath).c_str(), "r+");
				if (e == 0)
				{
					cf.ReadAllLines(_module);
				}
			}

			lerror CDefender::RunLoop()
			{
				////
				//	8
				//		进入主要逻辑，开始内部工作
				_sLog.WriteLog("step8 : into Main Loop\n");

				void* pv = NULL;

				lerror e = _pool.Query(&zoo::common::pool::POOL_OBJECT_FUNCTION,
								&zoo::pool::func::error_code::POOL_FUNCTION_ERROR_CODE, pv);
				if (e != 0)
				{
					_sLog.WriteLog("Query Error Code, Error\n");
				}
				else
				{
					_sLog.WriteLog("Query Error Code, Success\n");
					if (pv == NULL)
					{
						_sLog.WriteLog("Query Error Code, Success, Point NULL\n");
					}
					else
					{
						auto *iError = (zoo::pool::func::error_code::IErrorCode*)pv;
						const char* szErr = iError->GetErrMessage(0);
						_sLog.WriteLog(std::string("Query Error Code : 0, Msg : ").append(szErr).c_str());
					}
				}

				return 0;
			}
		}
	}
}

