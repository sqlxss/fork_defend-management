/**
 * @file error_code.h
 * @author sqlxss (276793422@qq.com)
 * @brief
 * @version 0.1
 * @date 2023-07-23
 *
 * Copyright (c) 2023 sqlxss (zhaojianzu)
 *
 */

#include "error_code.interface.h"
#pragma once


namespace zoo
{
	namespace pool
	{
		namespace func
		{
			namespace error_code
			{
				class CErrorCode : public IErrorCode
				{
				public:
					/**
					 * @brief 查询错误码内容
					 * @return 返回值只有两个，0 查询成功，1 不存在这个错误码
					 */
					virtual lerror Query(lerror uID, STRUCT_ERROR_CODE& info);

					/**
					 * @brief 查询错误码是否是个成功的返回值
					 * @return 返回1 为是，返回0 为否，如果没有对应ID，返回 -1
					 */
					virtual lerror IsSuccess(lerror uID);

					/**
					 * @brief 获取一个错误码的含义
					 * @return 这里返回值可能出现两个，一个是空字符串，一个是有内容的字符串
					 * @note 为了便于拼接，这里错误返回值没有返回 NULL ，如果有判断的需求，那么可以自行判断
					 */
					virtual const char* GetErrMessage(lerror uID);
				};
			}
		}
	}
}
