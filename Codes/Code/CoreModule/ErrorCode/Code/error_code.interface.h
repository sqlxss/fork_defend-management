/**
 * @file error_code.interface.h
 * @author sqlxss (276793422@qq.com)
 * @brief
 * @version 0.1
 * @date 2023-07-23
 *
 * Copyright (c) 2023 sqlxss (zhaojianzu)
 *
 */

#include "com_base.h"
#include "com_structure.h"
#pragma once

namespace zoo
{
	namespace pool
	{
		namespace func
		{
			namespace error_code
			{
				// {4C8A5400-FFFB-4309-AE26-DF7D8FBB0181}
				static DEFINE_GUID(POOL_FUNCTION_ERROR_CODE, 0x4c8a5400, 0xfffb, 0x4309, 0xae, 0x26, 0xdf, 0x7d, 0x8f, 0xbb, 0x1, 0x81);

				/**
				 * @brief 错误码值为正时为正确返回，否则为异常
				 * 			其他详细信息可以在列表中找到
				 */
				#pragma pack(1)
				struct STRUCT_ERROR_CODE
				{
					lerror lErrID;				///	错误码
					uint16_t uSuccess;			///	是否正确返回
					uint16_t uGroup;			///	错误模块组
					const char* szError;		///	错误码说明
				};
				#pragma pack()

				class IErrorCode
				{
				public:
					/**
					 * @brief 查询错误码内容
					 * @return 返回值只有两个，0 查询成功，1 不存在这个错误码
					 */
					virtual lerror Query(lerror uID, STRUCT_ERROR_CODE& info) = 0;

					/**
					 * @brief 查询错误码是否是个成功的返回值
					 * @return 返回1 为是，返回0 为否，如果没有对应ID，返回 -1
					 */
					virtual lerror IsSuccess(lerror uID) = 0;

					/**
					 * @brief 获取一个错误码的含义
					 * @return 这里返回值可能出现两个，一个是空字符串，一个是有内容的字符串
					 * @note 为了便于拼接，这里错误返回值没有返回 NULL ，如果有判断的需求，那么可以自行判断
					 */
					virtual const char* GetErrMessage(lerror uID) = 0;
				};
			}
		}
	}
}

