#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include "com_structure.h"
#include "com_path.h"
#include "com_log.h"
#include "com_memory.h"
#include "com_pool.interface.h"

#include "error_code.h"

/*
 *	这是个演示项目，主要演示了相关模块如何开发
 *	主要方法是在其他源文件中需要增加代码，然后在这里新增接口并且使用
 *
 * 	接口部分，入口点的传入参数是一个接口，使用方法，也在这里演示
 *
 * 	其他部分，主要是 interface.h 文件，必须以这个名字作为结尾
 * 	然后 makefile 会根据相关文件名，整理头文件到指定目录中，
 * 	然后就可以直接引用了
 */

zoo::common::pool::IPoolControler* _pPoolInterface = NULL;

zoo::pool::func::error_code::CErrorCode _ec;

extern "C" int __(void* pEnv)
{
	_pPoolInterface = (zoo::common::pool::IPoolControler*)pEnv;
	//printf("module : init : Env : 0x%X \n", pEnv);
	if (_pPoolInterface == NULL)
	{
		return 0x00000F01;
	}

	return _pPoolInterface->InsertObject(&zoo::common::pool::POOL_OBJECT_FUNCTION,
				0, 0, 0, &zoo::pool::func::error_code::POOL_FUNCTION_ERROR_CODE,
				&_ec);
}
