/**
 * @file error_code_map.h
 * @author sqlxss (276793422@qq.com)
 * @brief
 * @version 0.1
 * @date 2023-07-23
 *
 * Copyright (c) 2023 sqlxss (zhaojianzu)
 *
 */

#include "error_code.interface.h"
#pragma once



namespace zoo
{
	namespace pool
	{
		namespace func
		{
			namespace error_code
			{
				//	基础宏，用来生成一个错误码节点
				#define MAKE_ERROR_CODE(_ID, _SUCCESS, _MSG, _GROUP)	{(int)_ID, _SUCCESS, _GROUP, _MSG}
				//	生成一个正确返回的成功返回错误码
				#define MAKE_ERROR_TRUE_(_ID, _MSG, _GROUP)			{(int)_ID, 1, _GROUP, _MSG}
				//	生成一个正确返回的错误返回错误码
				#define MAKE_ERROR_FALSE(_ID, _MSG, _GROUP)			{(int)_ID, 0, _GROUP, _MSG}
				//	生成一个错误返回的错误码
				#define MAKE_ERROR_ERROR(_ID, _MSG, _GROUP)			{(int)_ID, 0, _GROUP, _MSG}
				//	生成一个错误返回的错误码
				#define MAKE_ERROR_ERR__(_ID, _MSG, _GROUP)			{(int)_ID, 0, _GROUP, _MSG}

				//	添加一个错误码，首先要保证错误码不重复
				//	然后根据自己的需求，添加一个指定的错误码
				//	添加错误码之前，需要确认自己的错误码的范围是否正确
				//	目前规划，每个范围内有 0xFFF 个错误值元素，即4096个错误码
				//	目前规划，可用错误码范围有 0x7FFFF 个，即 524287 个
				//	以上规划为 > 0 的情况下，正确的返回值
				//	返回值 < 0 的情况，原则上都视为错误返回值
				//	所以正常情况下，来说，只要判断一下返回值是否 >= 0 就知道返回值是否正确
				//		而更严格地情况下，只要判断返回值是否 == 0，就知道这个返回值是否正确

				static STRUCT_ERROR_CODE _err[] =
				{
					MAKE_ERROR_FALSE(0xFFFFFFFF, "异常失败，原因未知，建议程序结束", 0),

					//	0x00000000 ~ 0x00001000 - 1		常规系统错误
					MAKE_ERROR_TRUE_(0x00000000, "成功", 0),
					MAKE_ERROR_FALSE(0x00000001, "常规失败，原因未知", 0),
					MAKE_ERROR_FALSE(0x00000100, "Core Init Error , Env Error", 0),
					MAKE_ERROR_FALSE(0x00000F00, "Plugin 异常，这不是个标准Plugin ，是个Core", 0),
					MAKE_ERROR_FALSE(0x00000F01, "Plugin 异常，Plugin 获取 Env 参数错误，参数为 NULL", 0),

					//	0x00001000 ~ 0x00001100 - 1		池错误
					MAKE_ERROR_FALSE(0x00001000, "池元素已存在", 1),
					MAKE_ERROR_FALSE(0x00001001, "池元素插入，申请内存失败", 1),
					MAKE_ERROR_FALSE(0x00001002, "池元素不存在，没找到", 1),

					//	0x00001100 ~ 0x00001200 - 1		池管理器错误
					MAKE_ERROR_FALSE(0x00001100, "池管理器申请池失败", 2),
					MAKE_ERROR_FALSE(0x00001101, "池管理器查找池失败", 2),
					MAKE_ERROR_FALSE(0x00001102, "对应池对象不存在，无法插入", 2),
					MAKE_ERROR_FALSE(0x00001103, "对应池已经不存在，无法插入", 2),
					MAKE_ERROR_FALSE(0x00001104, "池管理器初始化完成，无法插入", 2),

					//	0x00001200 ~ 0x00001300 - 1		module 模块错误
					MAKE_ERROR_FALSE(0x00001200, "模块加载失败", 3),
					MAKE_ERROR_FALSE(0x00001201, "模块函数获取失败", 3),
					MAKE_ERROR_FALSE(0x00001202, "模块函数调用异常", 3),

					//	0x00001300 ~ 0x00001400 - 1		file 模块错误
					MAKE_ERROR_FALSE(0x00001300, "文件打开失败，已经打开文件了", 4),
					MAKE_ERROR_FALSE(0x00001301, "文件打开失败， fopen 失败", 4),
					MAKE_ERROR_FALSE(0x00001302, "指针错误，文件还未打开", 4),
					MAKE_ERROR_TRUE_(0x00001303, "文件结束", 4),
					MAKE_ERROR_TRUE_(0x00001304, "文件读取错误", 4),
					MAKE_ERROR_TRUE_(0x00001305, "文件不存在", 4),
					MAKE_ERROR_TRUE_(0x00001306, "文件删除失败", 4)
				};


			}
		}
	}
}



