#include "error_code.h"
#include "error_code_map.h"


namespace zoo
{
	namespace pool
	{
		namespace func
		{
			namespace error_code
			{
				/**
				 * @brief 查询错误码内容
				 * @return 返回值只有两个，0 查询成功，1 不存在这个错误码
				 */
				lerror CErrorCode::Query(lerror uID, STRUCT_ERROR_CODE& info)
				{
					for (auto it : _err)
					{
						if (it.lErrID == uID)
						{
							info = it;
							return 0;
						}
					}
					return 1;
				}

				/**
				 * @brief 查询错误码是否是个成功的返回值
				 * @return 返回1 为是，返回0 为否，如果没有对应ID，返回 -1
				 */
				lerror CErrorCode::IsSuccess(lerror uID)
				{
					for (auto it : _err)
					{
						if (it.lErrID == uID)
						{
							return it.uSuccess;
						}
					}
					return -1;
				}

				/**
				 * @brief 获取一个错误码的含义
				 * @return 这里返回值可能出现两个，一个是空字符串，一个是有内容的字符串
				 * @note 为了便于拼接，这里错误返回值没有返回 NULL ，如果有判断的需求，那么可以自行判断
				 */
				const char* CErrorCode::GetErrMessage(lerror uID)
				{

					for (auto it : _err)
					{
						if (it.lErrID == uID)
						{
							return it.szError;
						}
					}
					return "";
				}
			}
		}
	}
}


