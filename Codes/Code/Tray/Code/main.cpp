#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../../Common/include/com_memory.h"

int main()
{
	char *p = (char*)zoo::common::memory::Alloc(128);
	p[0] = '\0';

	strcpy(p, "123456");

	printf("%s\n", p);

	zoo::common::memory::Free(p);

	return 0;
}
